man_MANS      = inetd2rlinetd.8 rlinetd.8 rlinetd.conf.5
mansrcs       = inetd2rlinetd.8.in rlinetd.8.in rlinetd.conf.5.in
EXTRA_DIST    = $(mansrcs)          \
                po4a.cfg            \
                po/rlinetd-man.pot


# Extract the list of languages from the po4a config file.
LINGUAS       = `$(SED) -ne 's/^.*\[po4a_langs\] \(.*\)$$/\1/p' $(srcdir)/po4a.cfg`
stampfile     = $(srcdir)/po4a.stamp



MAN_TRANSFORM = $(SED) -e 's|%version%|$(VERSION)|g' \
                       -e 's|%sysconfdir%|$(sysconfdir)|g' \
                       -e 's|%libdir%|$(libdir)|g' \
                       -e 's|^.if *!.po4a.hide. *||'
$(man_MANS): % : %.in
	$(AM_V_at)  $(MKDIR_P) `dirname $@`
	$(AM_V_GEN) $(MAN_TRANSFORM) < $< > $@


PO4A_OPTIONS    = --msgid-bugs-address $(PACKAGE)@packages.debian.org   \
                  --package-name $(PACKAGE)                             \
                  --package-version $(VERSION)                          \
                  --copyright-holder='Robert Luberda <robert@debian.org> (msgids)' \
                  --previous                                            \
                  --variable srcdir=$(srcdir)                           \
                  --no-backups                                          \
                  --verbose

$(stampfile): $(srcdir)/po4a.cfg $(srcdir)/po/*.po $(mansrcs)
	$(AM_V_at) if [ "x$(po4apath)" != "x" ] ; then 		\
		$(po4apath) $(PO4A_OPTIONS) $(srcdir)/po4a.cfg;		\
		touch "$@";						\
	fi

update-po:
	$(AM_V_at) $(po4apath) $(PO4A_OPTIONS) --force $(srcdir)/po4a.cfg
	$(AM_V_at) touch "$(stampfile)"

all-local: all-local-@USE_NLS@
all-local-no: $(man_MANS)
all-local-yes: $(stampfile) all-local-no recurse-all-local-no

clean-local: clean-local-@USE_NLS@
clean-local-no:
	rm -f $(man_MANS)
clean-local-yes: clean-local-no recurse-clean-local-no

install-data-local: install-data-local-@USE_NLS@
install-data-local-no:
install-data-local-yes: recurse-install-man

uninstall-local: uninstall-local-@USE_NLS@
uninstall-local-no:
uninstall-local-yes: recurse-uninstall-man

recurse-all-local-no recurse-install-man: $(stampfile)
recurse-all-local-no recurse-clean-local-no recurse-install-man recurse-uninstall-man:
	$(AM_V_at) tgt=`echo $@ | $(SED) -e 's/^recurse-//'`; \
	for lang in $(LINGUAS); do \
		if [ -d $(srcdir)/$$lang ]; then \
			files=$$(cd $(srcdir) && echo $$lang/*.[1-9].in | $(SED) -e 's/\.in//g'); \
			$(MAKE) $$tgt  \
				mandir="$(mandir)/$$lang" \
				man_MANS="$$files"; \
		fi \
	done

.PHONY: recurse-all-local-no recurse-clean-local-no recurse-install-man recurse-uninstall-man

dist-hook: clean update-po
	for lang in $(LINGUAS); do \
		cp $(srcdir)/po/$$lang.po $(distdir)/po; \
		cp $(srcdir)/po/$$lang.add $(distdir)/po; \
		$(MKDIR_P) $(distdir)/$$lang; \
		cp -r $(srcdir)/$$lang $(distdir)/; \
	done
	cp po/rlinetd-man.pot $(distdir)/po
	cp $(stampfile) $(distdir)

.PHONY: update-po
