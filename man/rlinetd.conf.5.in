.\" vim:fdm=marker
.TH RLINETD.CONF 5 "November 11th, 2013" "Debian" "rlinetd %version%"
.SH NAME
rlinetd.conf \- rlinetd configuration file
.SH "DESCRIPTION"
.BR rlinetd.conf
holds configuration information for
.BR rlinetd .
There are a small number of similar top level constructs, differing
chiefly in which options can be meaningfully used with them.
.PP
All strings are quoted with the
.BR """
character. In some situations (e.g. the
.BR log ,
.BR exec ,
and
.BR chroot
directives), there are a number of variables that can be substituted
into the string.
.br
Unless stated otherwise, all numbers must be positive.
.PP
.\"
.\" --------------------------------------------------------------------
.\" {{{  service """name"""
.B service """name""" {
.RS
.B ...
.RE
.B }
.PP
.RS
.\"  service """name"""
This construct describes a service. The
.BR name
parameter is for naming convenience alone, it simply serves to
distinguish logging messages and provides a default for options which
can logically accept a name as an argument.
.PP
.\"
.\" --------------------------------------------------------------------
.\" {{{ service/enabled
.if !'po4a'hide' .BR enabled
.RS
.\" service/enabled
This construct allows easily enabling or disabling service.
The argument can be either
.B yes
or
.BR no .
The default value is
.BR yes .
Setting this to
.B no
disables service.

Example:
.\" start example
.RS 2
.if !'po4a'hide' .I enabled no;
.RE
.\" end example
.RE
.\" }}}
.\" --------------------------------------------------------------------
.\" {{{ service/port
.PP
.if !'po4a'hide' .BR port
.RS
.\" service/port
This lists the ports that the service should be made available on. The
ports can be listed in either string or numeric format. If
unspecified, this defaults to the name of the service unless the service
is an RPC service, in which case the port value will be dynamically assigned
by the system.
.PP
Example:
.\" start example
.RS 2
.if !'po4a'hide' .I "port ""telnet"", ""rcmd"", 56, 99;"
.RE
.\"end example
.RE
.\" }}}
.\" --------------------------------------------------------------------
.\" {{{ service/interface
.PP
.if !'po4a'hide' .BR interface
.RS
.\" service/interface
This specifies which interfaces the listed ports should be bound
on. It takes a list of IP addresses as an argument, corresponding to
the configured addresses of the interfaces required. If this keyword is
not given or when a special value
.B any
is assigned to it, the service will bind to all available interfaces.
.PP
Examples:
.\" start example
.RS 2
.if !'po4a'hide' .I interface 192.168.1.1, 192.168.1.2;
.PP
.if !'po4a'hide' .I interface any;
.RE
.\"end example
.RE
.\" }}}
.\" --------------------------------------------------------------------
.\" {{{ service/exec
.PP
.if !'po4a'hide' .BR exec
.RS
.\" service/exec
This specifies the invocation of the service. A number of
substitutions can be made within the string; please see
.B String Modifiers
below.
.PP
Example:
.\" start example
.RS 2
.if !'po4a'hide' .I exec """/usr/sbin/in.telnetd -d"";"
.RE
.\"end example
.RE
.\" }}}
.\" --------------------------------------------------------------------
.\" {{{ service/server
.PP
.if !'po4a'hide' .BR server
.RS
.\" service/server
This specifies the binary to be executed, if different to
.BR exec .
.PP
Example:
.\" start example
.RS 2
.if !'po4a'hide' .I server """/usr/sbin/tcpd"";"
.RE
.\"end example
.RE
.\" }}}
.\" --------------------------------------------------------------------
.\" {{{ service/protocol
.PP
.if !'po4a'hide' .BR protocol
.\" service/protocol
.RS
This specifies the socket protocol to use when listening on ports for
the service. The argument can be either
.B tcp
or
.BR udp .
The default setting for this variable is
.BR tcp .
.PP
Example:
.\" start example
.RS 2
.if !'po4a'hide' .I protocol tcp;
.RE
.\"end example
.RE
.\" }}}
.\" --------------------------------------------------------------------
.\" {{{ service/user
.PP
.if !'po4a'hide' .BR user
.\" service/user
.RS
This specifies the userid under which this service should run. It will
accept an argument in either symbolic or numeric form. Unless
.B group
(see below) is given, the groupid is also set to the user's primary group.
.PP
Example:
.\" start example
.RS 2
.if !'po4a'hide' .I user """nobody"";"
.RE
.\"end example
.RE
.\" }}}
.\" --------------------------------------------------------------------
.\" {{{ service/group
.PP
.if !'po4a'hide' .BR group
.RS
.\" service/group
This specifies the groupid under which this service should run. It
will accept an argument in either symbolic or numeric form.
.PP
Example:
.\" start example
.RS 2
.if !'po4a'hide' .I group """system"";"
.RE
.\"end example
.RE
.\" }}}
.\" --------------------------------------------------------------------
.\" {{{ service/backlog
.PP
.if !'po4a'hide' .BR backlog
.RS
.\" service/backlog
This is the backlog argument which will be passed to the
.BR listen (2)
system call.
.PP
Example:
.\" start example
.RS 2
.if !'po4a'hide' .I backlog 30;
.RE
.\"end example
.RE
.\" }}}
.\" --------------------------------------------------------------------
.\" {{{ service/instances
.PP
.if !'po4a'hide' .BR instances
.RS
.\" service/instances
This specifies the maximum number of service instances that can be
running at any one time. The default setting for this variable is 40.
.PP
Example:
.\" start example
.RS 2
.if !'po4a'hide' .I instances 50;
.RE
.\"end example
.RE
.\" }}}
.\" --------------------------------------------------------------------
.\" {{{ service/wait
.PP
.if !'po4a'hide' .BR wait
.RS
.\" service/wait
This directive emulates the
.BR inetd (8)
.I wait
behaviour.
The argument can be either
.B yes
or
.BR no .
The default value is
.BR no .
Setting this to
.B yes
also resets the value of
.B instances
option to 1.

Example:
.\" start example
.RS 2
.if !'po4a'hide' .I wait yes;
.RE
.\"end example
.RE
.\" }}}
.\" --------------------------------------------------------------------
.\" {{{ service/nice
.PP
.if !'po4a'hide' .BR nice
.RS
.\" service/nice
This specifies the process priority to run this service at. The
argument is passed directly to the
.BR setpriority (2)
system call. The value may be negative.
.PP
Example:
.\" start example
.RS 2
.if !'po4a'hide' .I nice \-5;
.RE
.\"end example
.RE
.\" }}}
.\" --------------------------------------------------------------------
.\" {{{ service/rpc
.PP
.if !'po4a'hide' .BR rpc
.RS
.\" service/rpc
This specifies that the service should be registered with the system's
.BR portmap (8)
mapper as an RPC service. It accepts a list of arguments as follows.
.PP
.B rpc {
.RS
.B name """string"";"
.B version 3,6,9\-15,22;
.RE
.B }
.PP
The
.I name
parameter is optional, and defaults to the service name.
.RE
.\" }}}
.\" --------------------------------------------------------------------
.\" {{{ service/chroot
.PP
.if !'po4a'hide' .BR chroot
.RS
.\" service/chroot
This specifies the root directory for the service. The string argument
can accept modifiers as detailed in
.B String Modifiers
below.
.PP
Example:
.\" start example
.RS 2
.if !'po4a'hide' .I chroot """/tftpboot/%O"";"
.RE
.\"end example
.RE
.\" }}}
.\" --------------------------------------------------------------------
.\" {{{ service/log
.PP
.if !'po4a'hide' .BR log
.RS
.\" service/log
This directive takes two arguments. The first must be either the
symbolic name of a previously specified
.BR log
directive (see below), or the unquoted word
.BR syslog .
If the latter, the message will be logged via the
.BR syslog (3)
call. The second argument is the message that will be logged, subject
to the modifiers detailed in
.B String Modifiers
below.
.PP
Example:
.\" start example
.RS 2
.I log syslog """Service from %O complete"";"
.RE
.\"end example
.RE
.\" }}}
.\" --------------------------------------------------------------------
.\" {{{ service/tcpd
.PP
.if !'po4a'hide' .BR tcpd
.RS
.\" service/tcpd
This  directive causes access controls as specified by
tcp_wrappers to be applied. This has the same effect as invoking a
service with a
.B server
argument of
.I /usr/sbin/tcpd
(or wherever your
.BR tcpd (8)
program is kept), but saves the additional step of starting the
program. It will accept up to two additional arguments. The first is a
service name to apply against its rules, and the second is a block of
instructions to execute if matched. If no name is specified, it
defaults to the name of the service. If the instruction block is not
specified, it defaults to 'exit;'.
.PP
Examples:
.\" start example
.RS 2
.nf
.if !'po4a'hide' .I tcpd """in.telnetd"";"
.PP
.if !'po4a'hide' .I tcpd { exec """/usr/local/bin/winnuke %O"";" }
.PP
.I tcpd """pointless""" { echo """Hi guys, come on in."";" }
.PP
.I tcpd """defiant""" { echo """500 Access denied from %O."";" exit; }
.fi
.RE
.\"end example
.RE
.\" }}}
.\" --------------------------------------------------------------------
.\" {{{ service/exit
.PP
.if !'po4a'hide' .BR exit
.RS
.\" service/exit
This directive is only useful in an instruction block argument to the
.B tcpd
directive. Note well - not using it (and not specifying some
other terminating directive, such as
.BR exec )
will result in the
service being run forever.
.PP
Example:
.\" start example
.RS 2
.if !'po4a'hide' .I exit;
.RE
.\"end example
.RE
.\" }}}
.\" --------------------------------------------------------------------
.\" {{{ service/capability
.PP
.if !'po4a'hide' .BR capability
.\" service/capability
.RS
This directive specifies the capabilities that this service should
have when running. The argument is a string that is passed directly to
.BR cap_from_text (3).
I know, that's a pretty lousy description, but this feature is of
limited utility until and unless you read the README.capabilities file
anyway.
.PP
Example:
.\" start example
.RS 2
.if !'po4a'hide' .I capability """cap_setuid=ep"";"
.RE
.\"end example
.RE
.\" }}}
.\" --------------------------------------------------------------------
.\" {{{ service/rlimit
.PP
.if !'po4a'hide' .BR rlimit
.RS
.\" service/rlimit
This directive takes two arguments. The first is a symbol specifying
the type of limit required. These are listed below. The second
argument takes one of two forms. It can either be a single numeric
value, in which case both of the soft and hard limits of the resource
in question will be set to this value. Alternatively, it can be a list
in the form:
.PP
.if !'po4a'hide' .B rlimit type {
.RS
.if !'po4a'hide' .B soft x;
.if !'po4a'hide' .B hard y;
.RE
.if !'po4a'hide' .B }
.PP
In which case the hard and soft limits will be set appropriately. In
either case, the word
.BR unlimited
can be specified instead of a numeric value, thus removing any
restriction. The values are passed directly to the
.BR setrlimit (2)
syscall, and should be specified in that context.
.PP
Types:
.RS 2
.if !'po4a'hide' .B cpu, fsize, data, stack, core, rss, nproc, nofile, memlock
.RE
.PP
Example:
.\" start example
.RS 2
.if !'po4a'hide' .I rlimit cpu 15;
.RE
.\"end example
.RE
.\" }}}
.\" --------------------------------------------------------------------
.\" {{{ service/initgroups
.PP
.if !'po4a'hide' .BR initgroups
.RS
.\" service/initgroups
The argument can be either
.B yes
or
.BR no .
This directive causes
.BR initgroups (3)
to be called at service startup, which sets the supplementary groups
of the service according to the
.B /etc/group
file.
.PP
Example:
.\" start example
.RS 2
.if !'po4a'hide' .I initgroups yes;
.RE
.\"end example
.RE
.\" }}}
.\" --------------------------------------------------------------------
.\" {{{ service/family
.PP
.if !'po4a'hide' .BR family
.RS
.\" service/family
This directive specifies the protocol family that rlinetd should bind
sockets on for this service. Currently, this can be either
.B ipv4
or
.B ipv6.
If unspecified, this defaults to something appropriate for the system.
.PP
Example:
.\" start example
.RS 2
.if !'po4a'hide' .I family ipv6;
.RE
.\"end example
.RE
.\" }}}
.\" --------------------------------------------------------------------
.\" {{{ service/banner
.PP
.if !'po4a'hide' .BR banner
.RS
.\" service/banner
This directive lets you dump a file as output to a connection.
.PP
Example:
.\" start example
.RS 2
.if !'po4a'hide' .I banner """/etc/nologin"";"
.RE
.\"end example
.RE
.\" }}}
.\" --------------------------------------------------------------------
.\" {{{ service/echo
.PP
.if !'po4a'hide' .BR echo
.RS
.\" service/echo
This directive allows you to output a dynamically generated line to
the connection.
.PP
Example:
.\" start example
.RS 2
.I echo """500 Service denied from your IP (%O)"";"
.RE
.\"end example
.RE
.\" }}}
.\" --------------------------------------------------------------------
.\" {{{ service/filter
.PP
.if !'po4a'hide' .BR filter
.RS
.\" service/filter
This directive allows you to specify a Linux Socket Filter program to
be associated with the listening socket. These can be generated with a
tool such as
.BR lsfcc (1).
.PP
Example:
.\" start example
.RS 2
.if !'po4a'hide' .I filter """/usr/local/lib/rlinetd/filters/privport"";"
.RE
.\"end example
.RE
.\" }}}
.\" --------------------------------------------------------------------
.\" {{{ service/chargen
.PP
.if !'po4a'hide' .BR chargen
.RS
.\" service/chargen
This directive loops eternally, outputting data to any connection. If
no argument is given, it echoes a subset of the printable
characters. However, a filename can be supplied as an argument, in
which case the contents of that file are output in a loop.
.PP
Example:
.\" start example
.RS 2
.if !'po4a'hide' .I chargen """/usr/local/lib/spam"";"
.RE
.\"end example
.RE
.\" }}}
.\" }}}
.\"
.\" ====================================================================
.\" {{{ log """name"""
.PP
.RE
.B log """name""" {
.RS
.B ...
.RE
.B }
.PP
.RS
.\" log """name"""
This construct describes a logging target. The
.BR name
parameter is used as an argument to the
.BR log
directive in
.BR service
configurations.
.\"
.\" ----------------------
.\" {{{ log/path
.PP
.if !'po4a'hide' .BR path
.RS
.\" log/path
This specifies the filename for this logfile.
.PP
Example:
.\" start example
.RS 2
.if !'po4a'hide' .I path """/var/log/service.log"";"
.RE
.\"end example
.RE
.\" }}}
.\" ----------------------
.\" {{{  log/mode
.PP
.if !'po4a'hide' .BR mode
.RS
.\" log/mode
This specifies the file permissions for the logfile. The argument is
required to be numeric, and defaults to 0640 if not specified.
.PP
Example:
.\" start example
.RS 2
.if !'po4a'hide' .I mode 0600;
.RE
.\"end example
.RE
.\" }}}
.\" ----------------------
.\" {{{ log/user
.PP
.if !'po4a'hide' .BR user
.RS
.\" log/user
This specifies the uid of the logfile, and can be specified as either
a numeric uid, or username.
.PP
Example:
.\" start example
.RS 2
.if !'po4a'hide' .I user """adm"";"
.RE
.\"end example
.RE
.\" }}}
.\" ----------------------
.\" {{{ log/group
.PP
.if !'po4a'hide' .BR group
.RS
.\" log/group
This specifies the gid of the logfile, and can be specified as either
a numeric gid, or groupname.
.PP
Example:
.\" start example
.RS 2
.if !'po4a'hide' .I group """adm"";"
.RE
.\"end example
.RE
.\" }}}
.\" }}}
.\"
.\" ====================================================================
.\" {{{ defaults
.PP
.RE
.PP
.B defaults {
.RS
.B ...
.RE
.B }
.PP
.RS
.\" defaults
This construct takes the same parameters as a
.BR service
declaration, but instead of specifying a service sets defaults for all
services specified subsequently.
.RE
.\"
.\" --------------------------------------------------------------------
.\" {{{ defaults/directory
.PP
.B directory """path""" """match""" """ignore"";"
.RS
.\" defaults/directory
This construct specifies a directory which contains additional
configuration files to be parsed. Parsing of these additional files
does not commence until the current file is complete. The
.IR match " and " ignore
arguments are optional, and if specified, are used to filter
the files in the directory. Filenames must match the
.I match
regexp, if given, and must not match the
.I ignore
regexp, if given. Filenames
beginning with a period ('.') are skipped in all cases. Directories
are not recursed into.
.RE
.\" }}}
.\" }}}
.\"
.\" ====================================================================
.\" {{{ String Modifiers
.PP
.SS String Modifiers
.PP
There are a number of variables which can be substituted into
arguments to some directives. Although they can all be used in the
same places, the information accessed by some is unavailable in
certain cases.
.PP
.TP
.if !'po4a'hide' .B %O
The source IP address of the connection.
.TP
.if !'po4a'hide' .B %P
The source port of the connection.
.TP
.if !'po4a'hide' .B %C
The total CPU time used.
.TP
.if !'po4a'hide' .B %U
The user CPU time used.
.TP
.if !'po4a'hide' .B %S
The system CPU time.
.TP
.if !'po4a'hide' .B %r
Maximum resident set size.
.TP
.if !'po4a'hide' .B %m
Shared memory size.
.TP
.if !'po4a'hide' .B %d
Unshared data size.
.TP
.if !'po4a'hide' .B %s
Unshared stack size.
.TP
.if !'po4a'hide' .B %f
Page reclaims.
.TP
.if !'po4a'hide' .B %F
Page faults.
.TP
.if !'po4a'hide' .B %p
Swaps.
.TP
.if !'po4a'hide' .B %i
Block input operations.
.TP
.if !'po4a'hide' .B %o
Block output operations.
.TP
.if !'po4a'hide' .B %n
Messages sent.
.TP
.if !'po4a'hide' .B %c
Messages received.
.TP
.if !'po4a'hide' .B %k
Signals received.
.TP
.if !'po4a'hide' .B %w
Voluntary context switches.
.TP
.if !'po4a'hide' .B %W
Involuntary context switches.
.TP
.if !'po4a'hide' .B %e
Exit code.
.TP
.if !'po4a'hide' .B %t
Running time.
.TP
.if !'po4a'hide' .B %M
The current time expressed as seconds since epoch 1980, dumped as a
network order 32 bit word. This has absolutely no use other than in
implementing the inetd-like time functionality.
.TP
.if !'po4a'hide' .B %I
The current time and date, in pretty-printed
.BR ctime (3)
format.
.\" }}}
.\"
.\" ====================================================================
.\"
.PP
.SH "SEE ALSO"
.if !'po4a'hide' .BR rlinetd (8),
.if !'po4a'hide' .BR hosts_access (5)
.SH AUTHOR
This manual page was written by Mikolaj J. Habryn <dichro-doc@rcpt.to>.
Modified by Robert Luberda <robert@debian.org>.
