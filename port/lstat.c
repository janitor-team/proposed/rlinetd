#if HAVE_CONFIG_H
# include <config.h>
#endif
#undef lstat

#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>

int rpl_lstat(const char *file_name, struct stat *buf)
{
	if ((!file_name) || (!*file_name)) {
		errno = EFAULT;
		return -1;
	}
	return lstat(file_name, buf);
}
