#if HAVE_CONFIG_H
# include <config.h>
#endif

#include <sys/types.h>

int memcmp(const void *s1, const void *s2, size_t n)
{
	const unsigned char *p = s1;
	const unsigned char *q = s2;

	while (n-- > 0) {
		if (*p++ != *q++)
			return (*--p - *--q);
	}
	return 0;
}

