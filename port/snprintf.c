#include <stdarg.h>
#include <stdio.h>
#include <sys/types.h>

#include <config.h>

int snprintf(char *buf, size_t size, const char *format, ...) {
	va_list argp;

	va_start(argp, format);
	return vsprintf(buf, format, argp);
}
