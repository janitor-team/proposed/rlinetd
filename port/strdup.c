#if HAVE_CONFIG_H
# include <config.h>
#endif

#include <stdlib.h>
#include <string.h>

char *strdup(const char *str) {
	char *tmp;

	tmp = malloc(strlen(str) + 1);
	if(!tmp)
		return NULL;
	strcpy(tmp, str);
	return tmp;
}
