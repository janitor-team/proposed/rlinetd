#include <stdarg.h>
#include <stdio.h>
#include <sys/types.h>

#include <config.h>

int vsnprintf(char *buf, size_t size, const char *format, va_list ap) {
	return vsprintf(buf, format, ap);
}
