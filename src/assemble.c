#include "config.h"

#include <stdarg.h>
#include <stdlib.h>
#include <string.h>

#include "assemble.h"
#include "bytecode.h"
#include "db.h"
#include "error.h"
#include "rlinetd.h"

/* last things that can possibly happen */
static rl_opcode_t terminals[] =
{ OP_CLOSE, OP_EXEC, OP_EXIT, OP_JUMP, OP_RET };
static struct array aterminals = {
#ifdef MEMDEBUG
	"static array",
#endif
	terminals, 5 };

/* anything likely to stop you doing any further privileged operations */
static rl_opcode_t restrictive[] =
{ OP_CLOSE, OP_EXEC, OP_EXIT, OP_JUMP, OP_RET, OP_SUID };
static struct array arestrictive = {
#ifdef MEMDEBUG
	"static array",
#endif
	restrictive, 6 };

/* things that have to happen after an initgroup() - other uid manipulations */
static rl_opcode_t initgr[] =
{ OP_CLOSE, OP_EXEC, OP_EXIT, OP_RET, OP_SGID, OP_SUID };
static struct array ainitgr = {
#ifdef MEMDEBUG
	"static array",
#endif
	initgr, 6 };

/* things that must not happen before a fork() */
static rl_opcode_t children[] =
{ OP_CLOSE, OP_CHROOT, OP_EXEC, OP_EXIT, OP_FORK, OP_NICE, OP_RET,
	OP_RLIMIT, OP_SGID, OP_SUID, OP_WRAP };
static struct array achildren = {
#ifdef MEMDEBUG
	"static array",
#endif
	children, 11 };

/* things that require a valid socket */
static rl_opcode_t sockops[] =
{ OP_BUFCOPY, OP_ECHO, OP_FORK, OP_BUFREAD, OP_BUFWRITE, OP_RHOOK,
	OP_RUNHOOK, OP_WHOOK, OP_WUNHOOK
};
static struct array asockops = {
#ifdef MEMDEBUG
	"static array",
#endif
	sockops, 9 };

/* things that a semaphore should protect - basically sockops + OP_ACCEPT */
static rl_opcode_t semkids[] = {
	OP_ACCEPT, OP_BUFCOPY, OP_ECHO, OP_FORK, OP_BUFREAD, OP_BUFWRITE, OP_RHOOK,
	OP_RUNHOOK, OP_WHOOK, OP_WUNHOOK
};
static struct array asemkids = {
#ifdef MEMDEBUG
	"static array",
#endif
	semkids, 10 };

static const struct optab optab[] = {
	{ 0, 0, NULL },								/* 0 */
	{ OP_EXEC, 2, NULL},
	{ OP_FISH, 0, &arestrictive },
	{ OP_SUID, 1, &aterminals },
	{ OP_SGID, 1, &arestrictive },
	{ OP_NICE, 1, &arestrictive },	/* 5 */
	{ OP_RLIMIT, 2, &arestrictive },
	{ OP_LOG, 1, &arestrictive },
	{ OP_CHROOT, 1, &arestrictive },
	{ OP_ACCEPT, 1, &asockops },
	{ OP_FORK, 2, &achildren },					/* 10 */
	{ OP_WRAP, 2, &arestrictive },
	{ OP_SETCAP, 1, &arestrictive },
	{ OP_INITGR, 2, &ainitgr },
	{ OP_BRANCH, 1, &aterminals },
	{ OP_CLOSE, 0, NULL },				/* 15 */
	{ OP_BUFCOPY, 1, &achildren },
	{ OP_ZERO, 0, &aterminals },
	{ OP_RET, 0, NULL },
	{ OP_EXIT, 0, NULL },
	{ OP_ECHO, 1, &aterminals },					/* 20 */
	{ OP_UP, 1, &aterminals },
	{ OP_DOWN, 1, &asemkids },
	{ OP_FROG, 0, &arestrictive },
	{ OP_LSET, 1, &arestrictive },
	{ OP_LCLR, 1, &arestrictive }, /* 25 */
	{ OP_BUFINIT, 1, &aterminals },
	{ OP_BUFREAD, 0, &aterminals },
	{ OP_BUFWRITE, 0, &aterminals },
	{ OP_RHOOK, 1, &aterminals },
	{ OP_RUNHOOK, 0, &aterminals },	/* 30 */
	{ OP_WHOOK, 1, &aterminals },
	{ OP_WUNHOOK, 0, &aterminals },
	{ OP_BZ, 1, &aterminals },
	{ OP_JUMP, 1, NULL },
	{ OP_BZNEG, 1, &aterminals },	/* 35 */
	{ OP_ADD, 0, &aterminals },
	{ OP_SUB, 0, &aterminals },
	{ OP_DUP, 0, &aterminals },
	{ OP_POP, 0, &aterminals },
	{ OP_BUFCLONE, 1, &aterminals }, /* 40 */
	{ OP_BUFFREE, 0, &aterminals },
	{ 0, 0, NULL }
};

static struct array *array_new(void) {
	struct array *tmp;

	tmp = (struct array *)malloc(sizeof(*tmp));
	if (!tmp)
		rl_fatal(EX_SOFTWARE, _("ABORT - Can't allocate memory"));

	memset(tmp, 0, sizeof(*tmp));
#ifdef MEMDEBUG
	tmp->id = strdup("array");
#endif
	return tmp;
}

static void array_free(struct array *a) {
	a->len = 0;
	if(a->elems)
		free(a->elems);
	a->elems = NULL;
#ifdef MEMDEBUG
	if(a->id) {
		free(a->id);
		a->id = NULL;
	} else {
		rl_warn(_("double free on struct array"));
	}
#endif
}

static void array_append(struct array *arr, rl_opcode_t op) {
	int i = arr->len;

	arr->elems = realloc(arr->elems, ++arr->len * sizeof(rl_opcode_t));
	if (!arr->elems)
		rl_fatal(EX_SOFTWARE, _("ABORT - Can't allocate memory"));
	arr->elems[i] = op;
}

static struct array *array_copy(struct array *a) {
	struct array *tmp;

	if(!a)
		return NULL;
	tmp = array_new();
	tmp->len = a->len;
	if(a->len) {
		tmp->elems = (rl_opcode_t *)malloc(sizeof(rl_opcode_t) * a->len);
		if (!tmp->elems)
			rl_fatal(EX_SOFTWARE, _("ABORT - Can't allocate memory"));
	}

	memcpy(tmp->elems, a->elems, sizeof(rl_opcode_t) * a->len);
	return tmp;
}

static struct array *array_union(struct array *a, struct array *b) {
	struct array *ret;
	int i, j;

	ret = array_copy(a);
	if(!ret)
		return array_copy(b);
	if(!b)
		return ret;
	for(i = 0; i < b->len; i++) {
		for(j = 0; j < ret->len; j++)
			if(ret->elems[j] == b->elems[i])
				goto found;
		array_append(ret, b->elems[i]);
found:
		continue;
	}
	return ret;
}

static int array_intersect_p(struct array *a, struct array *b) {
	int i, j;

	if(!a || !a->len || !b || !b->len)
		return 0;
	for(i = 0; i < a->len; i++)
		for(j = 0; j < b->len; j++)
			if(a->elems[i] == b->elems[j])
				return 1;
	return 0;
}

static struct opmeta *opmeta_new(int len) {
	struct opmeta *tmp;

	tmp = (struct opmeta *)malloc(sizeof(*tmp));
	if (!tmp)
		rl_fatal(EX_SOFTWARE, _("ABORT - Can't allocate memory"));
	memset(tmp, 0, sizeof(*tmp));
	tmp->after = array_new();
	tmp->ops = array_new();
	if(len) {
		tmp->fixup = (opmeta_fixup_ptr *)malloc(len * sizeof(opmeta_fixup_ptr));
		if (!tmp->fixup)
			rl_fatal(EX_SOFTWARE, _("ABORT - Can't allocate memory"));
	}
	memset(tmp->fixup, 0, len * sizeof(opmeta_fixup_ptr));
#ifdef MEMDEBUG
	tmp->id = strdup("opmeta");
#endif
	return tmp;
}

static void opmeta_free(struct opmeta *tmp) {
	if(!tmp)
		return;
	if(tmp->after) {
		array_free(tmp->after);
		free(tmp->after);
	}
	tmp->after = NULL;
	if(tmp->ops) {
		array_free(tmp->ops);
		free(tmp->ops);
	}
	tmp->ops = NULL;
	if(tmp->bytes)
		free(tmp->bytes);
	if(tmp->fixup)
		free(tmp->fixup);
	tmp->fixup = NULL;
	tmp->bytes = NULL;
	tmp->len = 0;
#ifdef MEMDEBUG
	if(tmp->id) {
		free(tmp->id);
		tmp->id = NULL;
	} else {
		rl_warn(_("double free on struct opmeta"));
	}
#endif
}

struct opmeta *opmeta_make(int len, rl_opcode_t op, ...) {
	struct opmeta *opm;
	va_list argp;
	int i;
	struct array *tmp;

	va_start(argp, op);
	opm = opmeta_new(len);
	do {
		i = optab[op].nargs;
 		tmp = array_union(opm->after, optab[op].after);
		if(opm->after) {
			array_free(opm->after);
			free(opm->after);
		}
		opm->after = tmp;
		opm->bytes = (rl_opcode_t *)
			realloc(opm->bytes, sizeof(rl_opcode_t) * (1 + i + opm->len));
		if (!opm->bytes)
			rl_fatal(EX_SOFTWARE, _("ABORT - Can't allocate memory"));
		array_append(opm->ops, op);
		do {
			opm->bytes[opm->len++] = op;
			op = va_arg(argp, rl_opcode_t);
			len--;
		} while(i--);
	} while(len > 0);
	va_end(argp);
	return opm;
}

void opmeta_fixup(struct opmeta *o, int offset, opmeta_fixup_ptr fixup) {
	o->fixup[offset] = fixup;
}

static void opmeta_resolve(struct opmeta *o, const opmeta_fixup_ptr fixups[]) {
	int i;

	for(i = 0; i < o->len; i++)
		if(o->fixup[i])
			o->bytes[i] = fixups[ o->fixup[i] ];
}

struct opmetalist *opmetalist_new() {
	struct opmetalist *opml;

	opml = (struct opmetalist *)malloc(sizeof(*opml));
	if (!opml)
		rl_fatal(EX_SOFTWARE, _("ABORT - Can't allocate memory"));
	memset(opml, 0, sizeof(*opml));
#ifdef MEMDEBUG
	opml->id = strdup("opmetalist");
#endif
	return opml;
}

static struct opmeta * opmeta_copy(struct opmeta *opm) {
	struct opmeta * o = opmeta_new(opm->len);

	if (opm->bytes) {
		o->bytes = malloc(sizeof(rl_opcode_t)*opm->len);
		memcpy(o->bytes, opm->bytes, sizeof(rl_opcode_t)*opm->len);
	}

	if (opm->fixup) memcpy(o->fixup, opm->fixup, sizeof(opmeta_fixup_ptr)*opm->len);
	array_free(o->after);
	array_free(o->ops);
	o->after = array_copy(opm->after);
	o->ops   = array_copy(opm->ops);
	o->len	 = opm->len;

	return o;

}


static int opmetalist_add_int(struct opmetalist *opml, struct opmeta *opm, int copy) {
	int i, idx;

	if(!opml)
		rl_fatal(EX_SOFTWARE, _("ABORT - opmetalist_add(NULL, ...)!"));
	if(!opm)
		return 0;
	for(i = 0; i < opml->len; i++)
		if(array_intersect_p(opm->after, opml->opms[i]->ops))
			break;
	idx = i;
	while(i < opml->len)
		if(array_intersect_p(opm->ops, opml->opms[i++]->after))
			return 1;
	opml->opms = realloc(opml->opms, ++opml->len * sizeof(*opml->opms));
	if (!opml->opms)
		rl_fatal(EX_SOFTWARE, _("ABORT - Can't allocate memory"));
	memmove(opml->opms + idx + 1, opml->opms + idx,
					sizeof(*opml->opms) * (opml->len - idx - 1));
	opml->opms[idx] = copy ? opmeta_copy(opm) :  opm;
	return 0;
}

int opmetalist_add(struct opmetalist *opml, struct opmeta *opm) {
	return opmetalist_add_int(opml, opm, 0);
}

void opmetalist_free(struct opmetalist *l) {
	while(l->len--) {
		opmeta_free(l->opms[l->len]);
		free(l->opms[l->len]);
	}
	free(l->opms);
	l->opms = NULL;
	l->len = 0;
#ifdef MEMDEBUG
	if(l->id) {
		free(l->id);
		l->id = NULL;
	} else {
		rl_warn(_("double free on struct opmetalist"));
	}
#endif
}

struct opmetalist *opmetalist_merge(struct opmetalist *o, const struct opmetalist *p) {
	if(!p)
		return o;
	int i = p->len;
	while (i--)
	{
		if (!p->opms[i]) continue;
		int j;
		for (j = 0; j < p->opms[i]->len; j++)
			rl_warn("--> %d\n", p->opms[i]->bytes[j]);
		opmetalist_add_int(o, p->opms[i], 1); /* ignore errors */
	}
	return o;
}

struct opmetalist *opmetalist_join(struct opmetalist *o, struct opmetalist *p) {
	if(!p)
		return o;
	while(p->len--)
		if(opmetalist_add(o, p->opms[p->len]))
			return NULL;
	p->len = 0;
	opmetalist_free(p);
	free(p);
	return o;
}

struct oplist *opmetalist_resolve(struct opmetalist *o, const opmeta_fixup_ptr fixups[]) {
	int i, j, k;
	struct oplist *ret;

	ret = (struct oplist *)malloc(sizeof(*ret));
	if (!ret)
		rl_fatal(EX_SOFTWARE, _("ABORT - Can't allocate memory"));
	ret->ops_len = 0;
	for(i = 0; i < o->len; i++) {
		opmeta_resolve(o->opms[i], fixups);
		ret->ops_len += o->opms[i]->len;
	}
	if(ret->ops_len) {
		ret->ops_list = (rl_opcode_t *)malloc(sizeof(*ret->ops_list) * ret->ops_len);
		if (!ret->ops_list)
			rl_fatal(EX_SOFTWARE, _("ABORT - Can't allocate memory"));
	}
	k = 0;
	for(i = 0; i < o->len; i++)
		for(j = 0; j < o->opms[i]->len; j++) {
#if 0
			rl_warn("rop %d", o->opms[i]->bytes[j]);
#endif
			ret->ops_list[k++] = o->opms[i]->bytes[j];
		}
	return ret;
}

/* vim: set ts=2: */
