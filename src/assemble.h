#ifndef H_ASSEMBLE
#define H_ASSEMBLE

#include "rlinetd.h"

struct array {
#ifdef MEMDEBUG
	char *id;
#endif	
	rl_opcode_t *elems;
	int len;
};

struct optab {
	rl_opcode_t opcode;
	int nargs;
	struct array *after;
};

typedef enum { ofp_none = 0,
	ofp_iname,	/* stringtab for service name */
	ofp_exec,	/* stringtab for program to exec */
	ofp_parent,	/* oplist for parent to exec post-fork */
	ofp_iuser,	/* stringtab for username */
	
	ofp_setuid,		/* user id */
	ofp_supgid,		/* supplementary groupid for initgroups */
	ofp_setgid,		/* primary group */
	ofp_onexit,	/* opstream to run when child exits */
						   ofp_max_fixups
						 } opmeta_fixup_ptr;

struct opmeta {
#ifdef MEMDEBUG
	char *id;
#endif	
	int len;
	rl_opcode_t *bytes;
	struct array *after;
	struct array *ops;
	opmeta_fixup_ptr *fixup;
};

struct opmetalist {
#ifdef MEMDEBUG
	char *id;
#endif	
	int len;
	struct opmeta **opms;
};


struct opmeta *opmeta_make(int, rl_opcode_t, ...);
int opmetalist_add(struct opmetalist *, struct opmeta *);
struct opmetalist *opmetalist_merge(struct opmetalist *o, const struct opmetalist *p);
struct opmetalist *opmetalist_join(struct opmetalist *, struct opmetalist *);
void opmeta_fixup(struct opmeta *, int, opmeta_fixup_ptr);
struct opmetalist *opmetalist_new(void);
struct oplist *opmetalist_resolve(struct opmetalist *, const opmeta_fixup_ptr[]);
void opmetalist_free(struct opmetalist *);

#endif /* !H_ASSEMBLE */

/* vim: set ts=2: */
