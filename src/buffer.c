#include "config.h"

#include <errno.h>
#include <stdlib.h>
#include <unistd.h>

#include "buffer.h"
#include "error.h"
#include "rlinetd.h"

static int do_read(int sock, void *buf, int len) {
	int err;
	
	err = read(sock, buf, len);
	if(err == -1) {
		switch(errno) {
			case EAGAIN:
				return 0;
				break;
			default:
				rl_warn("read(%d, %p, %d): %d (%s)", sock, buf, len,
								errno, strerror(errno));
				return -1;
		}
	}
	return err;
}
	
int rlbuf_read(int sock, struct rl_buffer *buf) {
	int done, err, size;
	
	if(!buf) {
		rl_warn(_("%s called with NULL buf"), "rlbuf_read");
		return 0;
	}
	done = 0;
	err = 0;
	if(buf->head >= buf->tail) {
		size = buf->size - buf->head - !buf->tail;
		if(size) {
			if((err = do_read(sock, buf->data + buf->head, size)) < 0)
				return err;
			buf->head += err;
			done += err;
			if(err < size)
				return err;
			if(buf->tail)
				buf->head = 0;
		}
	}
	if(buf->tail && (buf->head < buf->tail)) {
		size = buf->tail - 1;
		if(size) {
			err = do_read(sock, buf->data, size);
			if(err < 0)
				err = 0;
			buf->head += err;
			done += err;
		}
	}
	return done;
}

static int do_write(int sock, void *buf, int len) {
	int err;
	
	err = write(sock, buf, len);
	if(err == -1) {
		switch(errno) {
			case EAGAIN:
				return 0;
				break;
			default:
				rl_warn("write(%d, %p, %d): %d (%s)", sock, buf, len,
								errno, strerror(errno));
				/* fallthrough */
			case EPIPE:
				return -1;
		}
	}
	return err;
}

int rlbuf_write(int sock, struct rl_buffer *buf) {
	int done, err, size;

	if(!buf) {
		rl_warn(_("%s called with NULL buf"), "rlbuf_write");
		return 0;
	}
	done = 0;
	err = 0;
	if(buf->head == buf->tail)
		return 0;
	if(buf->head > buf->tail)
		size = buf->head - buf->tail;
	else
		size = buf->size - buf->tail;
	if(size) {
		if((err = do_write(sock, buf->data + buf->tail, size)) < 0)
			return err;
		buf->tail += err;
		done += err;
		if(err < size)
			return err;
		if(buf->tail == buf->size)
			buf->tail = 0;
	}
	if(buf->tail < buf->head) {
		size = buf->head - buf->tail;
		err = do_write(sock, buf->data + buf->tail, size);
		if(err < 0)
			err = 0;
		buf->tail += err;
		done += err;
	}
	return done;
}

int rlbuf_copy(int sock, struct rl_buffer *buf, int offset) {
	int done, err, size;

	if(!buf) {
		rl_warn(_("%s called with NULL buf"), "rlbuf_copy");
		return 0;
	}
	done = 0;
	err = 0;
	if(buf->head == buf->tail)
		return 0;
	if(buf->head > buf->tail)
		size = buf->head - buf->tail;
	else
		size = buf->size - buf->tail;
	if(size <= offset) {
		offset -= size;
		size = 0;
	} 
	if(size) {
		if((err = do_write(sock, buf->data + buf->tail + offset, size)) < 0)
			return err;
		done += err;
		if(err < size)
			return err;
	}
	offset -= done;
	if(offset < 0)
		offset = 0;
	if(buf->tail > buf->head) {
		size = buf->head - offset;
		if(size <= 0)
			return 0;
		err = do_write(sock, buf->data + offset, size);
		if(err < 0)
			err = 0;
		done += err;
	}
	return done;
}

void rlbuf_init(struct rl_buffer **buf, int size) {
	if(!*buf) {
		*buf = malloc(sizeof(**buf));
		if (!(*buf))
			rl_fatal(EX_SOFTWARE, _("ABORT - Can't allocate memory"));
		memset(*buf, 0, sizeof(**buf));
	} else {
		(*buf)->head = (*buf)->tail = 0;
	}
	if((*buf)->data) {
		free((*buf)->data);
		(*buf)->data = NULL;
	}
	if(size) {
		(*buf)->size = size;
		(*buf)->data = malloc(size);
		if (!(*buf)->data)
			rl_fatal(EX_SOFTWARE, _("ABORT - Can't allocate memory"));
	}
}
	
/* vim: set ts=2: */
