#ifndef H_BUFFER
#define H_BUFFER

struct rl_buffer {
	char *data;
	int head, tail;
	int size;
};

int rlbuf_read(int, struct rl_buffer *);
int rlbuf_write(int, struct rl_buffer *);
int rlbuf_copy(int, struct rl_buffer *, int);
void rlbuf_init(struct rl_buffer **, int);

#endif /* H_BUFFER */

/* vim: set ts=2: */
