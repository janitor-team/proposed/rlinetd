#include "config.h"

#include <errno.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <string.h>
#include <sys/socket.h>

#include "error.h"
#include "rlinetd.h"
#if 0
int rl_connect(struct rl_instance *inst, int family, int type,
							 int proto, struct sockaddr *saddr, int saddrlen,
							 struct sockaddr *daddr, int daddrlen)
{
	int fd, ret;
	long opt = 1;

	fd = socket(family, type, proto);
	if(fd < 0)
		return fd;
	if(fcntl(fd, F_SETFL, O_NDELAY)) {
		rl_warn("fcntl(%d, F_SETFL, O_NDELAY): %s",	fd, strerror(errno));
		close(fd);
		return -1;
	}
	if(saddr) {
		if(setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt)))
			rl_warn("setsockopt(%d, SOL_SOCKET, SO_REUSEADDR, 1): %s",
							fd, strerror(errno));
		if(bind(fd, saddr, saddrlen))
			rl_warn("bind(): %s", strerror(errno));
	}
	if((ret = connect(fd, daddr, daddrlen)) && (ret != EINPROGRESS)) {
		rl_warn("connect(): %s", strerror(errno));
		close(fd);
		return -1;
	}
	return 0;
}
#endif
/* vim: set ts=2: */
