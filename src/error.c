#include "config.h"

#include <syslog.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>

#include "error.h"
#include "rlinetd.h"

static void rl_log(int, const char *, int, const char *, va_list);

void rl_note(const char * fmt, ...) {
	va_list argp;

	va_start(argp, fmt);
	rl_log(LOG_INFO, NULL, 0, fmt, argp);
	va_end(argp);
}

void rl_warn(const char * fmt, ...) {
	va_list argp;

	va_start(argp, fmt);
	rl_log(LOG_WARNING, NULL, 0, fmt, argp);
	va_end(argp);
}

void rl_pwarn(const char * file, int line, const char * fmt, ...) {
	va_list argp;

	va_start(argp, fmt);
	rl_log(LOG_WARNING, file, line, fmt, argp);
	va_end(argp);
}


void rl_fatal(int ex, const char * fmt, ...) {
	va_list argp;

	va_start(argp, fmt);
	rl_log(LOG_ERR, NULL, 0, fmt, argp);
	va_end(argp);

	exit(ex);
}

void rl_pfatal(int ex, const char * file, int line, const char * fmt, ...) {
	va_list argp;

	va_start(argp, fmt);
	rl_log(LOG_ERR, file, line, fmt, argp);
	va_end(argp);

	exit(ex);
}



static void rl_log(int level, const char * file, int line, const char * fmt, va_list argp) {
	char message[1024];
	int cur_len;
	int max_len;


	max_len 	= sizeof(message) - 1;
	cur_len 	= 0;
	
	memset(message, 0, max_len);

	if (file) {
		snprintf(message, max_len, "(%.255s:%d) ", 
				file, line);
		cur_len = strlen(message);
		max_len -= (cur_len + 1);
	}

	vsnprintf(message + cur_len, max_len, fmt, argp);
	
	if (rl_debug)
	{
		fprintf(stderr, "%s\n", message);
		fflush(stderr);
	}	
	else
		syslog(level, "%s", message);
}


/* vim: set ts=2: */
