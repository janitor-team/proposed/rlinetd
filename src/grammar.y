%{
#include "config.h"

#include <errno.h>
#include <fcntl.h>
#include <grp.h>
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <pwd.h>

#ifdef HAVE_RPC_RPC_H
# include <rpc/rpc.h>
#endif
#ifdef HAVE_RPC_PMAP_CLNT_H
# include <rpc/pmap_clnt.h>
#endif

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sysexits.h>
#include <syslog.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <regex.h>

#include "installpaths.h"
#include "port/port.h"

#if HAVE_DIRENT_H
# include <dirent.h>
# define NAMLEN(dirent) strlen((dirent)->d_name)
#else
# define dirent direct
# define NAMLEN(dirent) (dirent)->d_namlen
# if HAVE_SYS_NDIR_H
#  include <sys/ndir.h>
# endif
# if HAVE_SYS_DIR_H
#  include <sys/dir.h>
# endif
# if HAVE_NDIR_H
#  include <ndir.h>
# endif
#endif

#ifdef HAVE_NET_BPF_H
# include <net/bpf.h>
#endif

#include "assemble.h"
#include "bytecode.h"
#include "data.h"
#include "db.h"
#include "error.h"
#include "grammar.h"
#include "lex.h"
#include "libdb.h"
#include "parse.h"
#include "rlinetd.h"
#include "signals.h"
#include "util.h"

#define RL_PWARN(x...)			rl_pwarn(curfile_name, curfile_line, x)
#define RL_PFATAL(x,y...)		rl_pfatal(x, curfile_name, curfile_line, y)

struct userdata *userdata;

static struct service *current_service;
extern FILE *yyin;
extern char *rl_config;
char **files = NULL;
static int curfile = -1, numfiles = 0;
char *curfile_name = NULL;  /* name of the current file */
int curfile_line = 1;					 /* line */

static struct service *defaults;
static struct opmetalist *opml_defaults;

static struct logdata *logcur, *logdatas;

static struct numlist *numlist = NULL;
static struct stringlist *stringlist = NULL;

extern void yyerror(const char *c);
extern int yywrap(void);
extern int yylex(void);
static int bind_ports(void);
static int add_user_group(const struct service* current_service, struct opmetalist * l);
static void service_copy(struct service *to, struct service *from);
static void service_free(struct service *s);
static struct service *service_new(void);
#ifndef HAVE_GETADDRINFO
static int getservport(char *str, char *proto);
#endif
static struct logdata *logdata_get(char *name);
static struct logdata *logdata_new(void);
static void pidtab_fixup(void);
static void validate_service(struct service *s);
static void add_directory(char *dir, char *match, char *ignore);
static int chargen_buffer(void);
%}

%union {
	long num;
	char *cp;
	struct passwd *uid;
	struct group *gid;
	struct opmetalist *opml;
	struct opmeta *opm;
	rlim_t rl;
}

%token T_SERVICE T_PORT T_EXEC T_PROTO T_UDP T_TCP T_UID T_GID T_BACKLOG
%token T_INSTANCES T_WAIT T_DEFAULT T_NICE T_INTERFACE T_ANY T_FISH T_CHROOT T_SERVER
%token T_DIR T_RPC T_VERSION T_NAME T_WRAP T_CAPS T_FAMILY T_IPV4 T_IPV6
%token T_INITGROUPS T_BANNER T_ECHO T_DISCARD T_FILTER T_CHARGEN T_LOOPBACK
%token T_ENABLED

%token T_RLIMIT T_LIM_CPU T_LIM_FSIZE T_LIM_DATA T_LIM_STACK T_LIM_CORE
%token T_LIM_RSS T_LIM_NPROC T_LIM_NOFILE T_LIM_MEMLOCK T_LIM_INFINITY
%token T_SOFT T_HARD
%token T_YES T_NO

%token T_LOG T_PIPE T_SYSLOG T_PATH T_MODE

%token T_ACCEPT T_DENY T_CLOSE T_EXIT

%token <num> T_NUMERIC
%token <cp> T_QSTRING T_IPADDR

%type <gid> groupid;
%type <uid> userid;
%type <num> limittype limit protocol;
%type <opm> opcode_element
%type <opml> service_elements complex_opcode_element opcode_block
%type <rl> limitvalue
%type <cp> optionalstring
%type <num> signed_numeric
%type <num> boolean

%%

tles:	tle
	|	tles tle
	;

tle:	service
	|	default
	|	log
	|	directory
	;

directory:	T_DIR T_QSTRING ';'
	{	add_directory($2, NULL, NULL);	}
	|	T_DIR T_QSTRING T_QSTRING ';'
	{	add_directory($2, $3, NULL);	}
	|	T_DIR T_QSTRING T_QSTRING T_QSTRING ';'
	{	add_directory($2, $3, $4);	}
	;

log:	T_LOG T_QSTRING '{' log_element '}'
	{
		if(logdata_get($2)) {
			RL_PWARN("duplicate declaration of log %s, ignoring", $2);
			free($2);
		} else {
			logcur->name = $2;
			logcur->next = logdatas;
			logdatas = logcur;
			logcur = logdata_new();
		}
		clearuserdata(&userdata);
	}
	;

log_element: file_elements
	{
		if(!logcur->path) {
			RL_PWARN(_("No path defined for log %s"), logcur->name);
		} else {
			if((logcur->index = open(logcur->path, O_CREAT|O_APPEND|O_WRONLY, logcur->mode)) < 0) {
				RL_PWARN(_("open(\"%s\", O_CREAT|O_APPEND|O_WRONLY, 0%o) failed (%s)"),
								logcur->path, logcur->mode, strerror(errno));
			} else {

				if(fchmod(logcur->index, logcur->mode)) {
					RL_PWARN(_("fchmod(\"%s\", 0%o) failed (%s)"),
									logcur->path, logcur->mode,
									strerror(errno));
				}

				if(fchown(logcur->index, logcur->uid, logcur->gid)) {
					RL_PWARN(_("fchown(\"%s\", %d, %d) failed for log %s (%s)"),
									logcur->path, logcur->uid, logcur->gid, logcur->name,
									strerror(errno));
				}
			}
		}
	}
	;

file_elements:	file_element
	|	file_elements file_element
	;

file_element:	';'
	|	T_PATH T_QSTRING ';'
	{
		if(logcur->path) {
			RL_PWARN(_("duplicate path declaration (%s) "
				"in log directive %s, ignoring"), $2,
				logcur->name);
			free($2);
		} else {
			logcur->path = $2;
		}
	}
	|	T_UID userid ';'
	{
		if ($2) {
			logcur->uid = $2->pw_uid;
		} else {
			RL_PWARN("unknown username");
		}
	}
	|	T_GID groupid ';'
	{
		if ($2) {
			logcur->gid = $2->gr_gid;
		} else {
			RL_PWARN("unknown groupname");
		}
	}
	|	T_MODE T_NUMERIC ';'
	{
		if ($2 > 07777)  {
			RL_PWARN(_("invalid mode declaration "
				"in log directive %s, ignoring"), logcur->name);
		} else {
			logcur->mode = $2;
		}
	}
	;

default:	T_DEFAULT '{' service_elements '}'
	{
		/* swich pointers to current_service and defaults */
		struct service * saved_defaults = defaults;
		defaults = current_service;
		current_service = saved_defaults;

		/* copy new defaults to current_service */
		service_free(current_service);
		service_copy(current_service, defaults);

		/* save default opmetalist */
		opmetalist_free(opml_defaults);
		free(opml_defaults);
		opml_defaults = $3;
	}
	;

service:	T_SERVICE T_QSTRING '{' service_elements '}'
	{
		struct opmetalist *parent;
		struct opmetalist *onexit;
		struct oplist *ops;
		int i;
		int fds;
		fd_set *fdst;

		parent = opmetalist_new();
		onexit = opmetalist_new();

		current_service->name = $2;
		current_service->opfixups[ofp_iname] = stringtab_add($2);

		do { /* avoid goto */

			if (current_service->disabled)
				break;

			if(current_service->socktype == SOCK_STREAM && !current_service->wait) {
				if(opmetalist_add($4, opmeta_make(2, OP_ACCEPT, 200))) { /* !200 */
					RL_PWARN(_("opcode resolving problem"));
					current_service->disabled++;
					break;
				}
				opmetalist_add(parent, opmeta_make(1, OP_CLOSE));

				if (current_service->internal)
					opmetalist_add(onexit, opmeta_make(1, OP_CLOSE));
				else
					opmetalist_add(onexit, opmeta_make(1, OP_RET));

			} else {
				opmetalist_add(parent, opmeta_make(1, OP_RET));
				opmetalist_add(onexit, opmeta_make(1, OP_RET));
			}
			validate_service(current_service);


			if (current_service->disabled)
				break;

			if (!add_user_group(current_service, $4))
			{
				++current_service->disabled;
			  break;
		  }


			fds = bind_ports();


			if(current_service->limit) {
				struct opmetalist *o;
				struct oplist *l;
				int match, under;

				o = opmetalist_new();
				opmetalist_add(o, opmeta_make(2, OP_LCLR, fds));
				opmetalist_add(o, opmeta_make(1, OP_RET));
				l = opmetalist_resolve(o, current_service->opfixups);
				match = oplisttab_add(l);
				opmetalist_free(o);
				free(o);
				oplist_free(l);
				free(l);

				o = opmetalist_new();
				opmetalist_add(o, opmeta_make(2, OP_LSET, fds));
				opmetalist_add(o, opmeta_make(1, OP_RET));
				l = opmetalist_resolve(o, current_service->opfixups);
				under = oplisttab_add(l);
				opmetalist_free(o);
				free(o);
				oplist_free(l);
				free(l);

				i = semaphore_add(current_service->limit, match, under);
				opmetalist_add($4, opmeta_make(2, OP_DOWN, i));
				opmetalist_add(onexit, opmeta_make(2, OP_UP, i));
			}
			ops = opmetalist_resolve(onexit, current_service->opfixups);
			current_service->opfixups[ofp_onexit] = oplisttab_add(ops);
			opmetalist_free(onexit);
			free(onexit);
			oplist_free(ops);
			free(ops);

			ops = opmetalist_resolve(parent, current_service->opfixups);
			current_service->opfixups[ofp_parent] = oplisttab_add(ops);
			opmetalist_free(parent);
			free(parent);
			oplist_free(ops);
			free(ops);

			opmetalist_merge($4, opml_defaults);

			opmetalist_add($4, opmeta_make(2, OP_JUMP, current_service->opfixups[ofp_onexit]));
			ops = opmetalist_resolve($4, current_service->opfixups);

			current_service->run = oplisttab_add(ops);
			opmetalist_free($4);
			free($4);
			oplist_free(ops);
			free(ops);

			fdst = fdsettab_get(fds);
			for(i = 0; i < FD_SETSIZE; i++)
				if(FD_ISSET(i, fdst))
					read_hook(i, oplisttab_get(current_service->run), NULL);

		} while (0);

		if (current_service->disabled) {
					rl_warn(_("service %s DISABLED"), current_service->name);
		} else {
			rl_note(_("service %s enabled"), current_service->name);
			if (rl_debug > 1)
				rl_note(_("+-> uid=%d, sgid=%d, supgid=%d fl=%d"), current_service->opfixups[ofp_setuid],
					current_service->opfixups[ofp_setgid], current_service->opfixups[ofp_supgid], current_service->sflags);

		}

		service_free(current_service);
		service_copy(current_service, defaults);
	}
	;

service_elements:	service_element
	{
		$$ = opmetalist_new();
	}
	| service_elements service_element
	{
		$$ = $1;
	}
	| opcode_element
	{
		struct opmetalist *l;

		l = opmetalist_new();
		if(opmetalist_add(l, $1)) {
			RL_PWARN(_("opcode resolving problem"));
			current_service->disabled++;
		}
		$$ = l;
	}
	| service_elements opcode_element
	{
		if(opmetalist_add($1, $2)) {
			RL_PWARN(_("opcode resolving problem"));
			current_service->disabled++;
		}
		$$ = $1;
	}
	| service_elements complex_opcode_element
	{
		$$ = opmetalist_join($1, $2);
		if(!$$) {
			RL_PWARN(_("opcode resolving problem"));
			current_service->disabled++;
		}
	}
	| complex_opcode_element
	{
		$$ = $1;
	}
	;

opcode_block: complex_opcode_element
	{
		$$ = $1;
	}
	|	opcode_element
	{
		struct opmetalist *l;

		l = opmetalist_new();
		if(opmetalist_add(l, $1)) {
			RL_PWARN(_("opcode resolving problem"));
			current_service->disabled++;
		}
		$$ = l;
	}
	| opcode_block opcode_element
	{
		if(opmetalist_add($1, $2)) {
			RL_PWARN(_("opcode resolving problem"));
			current_service->disabled++;
		}
		$$ = $1;
	}
	| opcode_block complex_opcode_element
	{
		$$ = opmetalist_join($1, $2);
		if(!$$) {
			RL_PWARN(_("opcode resolving problem"));
			current_service->disabled++;
		}
	}
	;

complex_opcode_element:	T_EXEC T_QSTRING ';'
	{
		struct opmetalist *l;
		struct opmeta *o;

		l = opmetalist_new();
		o = opmeta_make(3, OP_EXEC, 666, argvtab_add($2, 1));
		opmeta_fixup(o, 1, ofp_exec);
		if(opmetalist_add(l, o)) {
			RL_PWARN(_("opcode resolving problem"));
			current_service->disabled++;
		}
		o = opmeta_make(3, OP_FORK, 666, 666);
		opmeta_fixup(o, 1, ofp_parent);
		opmeta_fixup(o, 2, ofp_onexit);
		if(opmetalist_add(l, o)) {
			RL_PWARN(_("opcode resolving problem"));
			current_service->disabled++;
		}
		$$ = l;
	}
	|	T_UID userid ';'
	{
		if($2) {

			current_service->opfixups[ofp_iuser] = stringtab_add($2->pw_name);
			current_service->opfixups[ofp_setuid] = $2->pw_uid;
			current_service->opfixups[ofp_supgid] = $2->pw_gid;

			set_flag(current_service, FLAG_USER);

			if(!has_flag(current_service, FLAG_GROUP)) {
				current_service->opfixups[ofp_setgid] = $2->pw_gid;
			}

			$$ = NULL;
		} else {
			RL_PWARN("unknown username");
			current_service->disabled++;
			$$ = NULL;
		}
	}
	| T_INITGROUPS ';'
	{
		set_flag(current_service, FLAG_INITGROUPS);

		$$ = NULL;

	}
	| T_INITGROUPS boolean ';'
	{
		if ($2)
			set_flag(current_service, FLAG_INITGROUPS);
		else
			reset_flag(current_service, FLAG_INITGROUPS);
		$$ = NULL;
	}
	;

opcode_element:	T_NICE signed_numeric ';'
	{
		$$ = opmeta_make(2, OP_NICE, $2);
	}
	|	T_FISH ';'
	{
		$$ = opmeta_make(1, OP_FISH);
	}
	|	T_RLIMIT limittype limit
	{
		$$ = opmeta_make(3, OP_RLIMIT, $2, $3);
	}
	|	T_LOG T_SYSLOG T_QSTRING ';'
	{
		int i;

		i = logtab_add(-1, $3);
		$$ = opmeta_make(2, OP_LOG, i);
		free($3);
	}
	|	T_BANNER T_QSTRING ';'
	{
		int ret;

		ret = buftab_addfile($2);
		if(ret == -1)
			current_service->disabled++;
		$$ = opmeta_make(16, OP_ZERO, OP_BUFCLONE, ret, OP_WHOOK, 0, OP_WUNHOOK,
										 OP_DUP, OP_BUFCOPY, OP_DUP, OP_BZNEG, 3, OP_ADD,
										 OP_WHOOK, -9, OP_POP, OP_BUFFREE);
		free($2);
	}
	|	T_CHARGEN optionalstring ';'
	{
		int i;

		if($2) {
			i = buftab_addfile($2);
			if(i < 0)
				current_service->disabled++;
		} else {
			i = chargen_buffer();
		}
		$$ = opmeta_make(25, OP_ZERO, OP_DUP, OP_BUFCLONE, i, OP_POP,
										 OP_POP, OP_ZERO, OP_WHOOK, 0, OP_WUNHOOK, OP_DUP,
										 OP_BUFCOPY, OP_DUP, OP_BZ, -11, OP_DUP, OP_BZNEG, 3,
										 OP_ADD, OP_WHOOK, -12, OP_POP, OP_BUFFREE, OP_JUMP, 666);
		opmeta_fixup($$, 24, ofp_onexit);
		current_service->internal = 1;
		if($2)
			free($2);
	}
	|	T_LOG T_QSTRING T_QSTRING ';'
	{
		struct logdata *ld = logdata_get($2);

		if(!ld) {
			RL_PWARN(_("unknown log %s"), $2);
			current_service->disabled++;
		} else {
			int i = logtab_add(ld->index, $3);
			$$ = opmeta_make(2, OP_LOG, i);
		}
		free($2);
		free($3);
	}
	|	T_GID groupid ';'
	{
		if(!$2) {
			RL_PWARN(_("unknown group"));
			current_service->disabled++;
			$$ = NULL;
		} else {

			current_service->opfixups[ofp_setgid] = $2->gr_gid;
			set_flag(current_service, FLAG_GROUP);

			$$ = NULL;
		}
	}
	|	T_CHROOT T_QSTRING ';'
	{
		$$ = opmeta_make(2, OP_CHROOT, argvtab_add($2, 1));
	}
	|	T_CAPS T_QSTRING ';'
	{
#ifdef HAVE_CAPABILITIES
		cap_t current_caps = cap_from_text($2);
		if(current_caps) {
			$$ = opmeta_make(2, OP_SETCAP, captab_add(current_caps));
		} else {
			RL_PWARN(_("failed to parse capability string \"%s\"\n"), $2);
			current_service->disabled++;
		}
		free($2);
#else
		RL_PFATAL(EX_DATAERR, _("ABORT - support for capabilities not compiled in"));
#endif
	}
	| T_WRAP T_QSTRING '{' opcode_block '}'
	{
		int i;
		struct oplist *op;

		opmetalist_add($4, opmeta_make(1, OP_RET));
		op = opmetalist_resolve($4,current_service->opfixups);
		i = oplisttab_add(op);
		$$ = opmeta_make(3, OP_WRAP, stringtab_add($2), i);
		free($2);
		oplist_free(op);
		free(op);
		opmetalist_free($4);
		free($4);
	}
	| T_WRAP '{' opcode_block '}'
	{
		struct opmeta *o;
		int i;
		struct oplist *op;

		opmetalist_add($3, opmeta_make(1, OP_RET));
		op = opmetalist_resolve($3, current_service->opfixups);
		i = oplisttab_add(op);
		o = opmeta_make(3, OP_WRAP, 666, i);
		opmeta_fixup(o, 1, ofp_iname);
		$$ = o;
		oplist_free(op);
		free(op);
		opmetalist_free($3);
		free($3);
	}
	|	T_WRAP T_QSTRING ';'
	{
		struct opmetalist *l;
		int i;
		struct oplist *op;

		l = opmetalist_new();
		opmetalist_add(l, opmeta_make(1, OP_EXIT));
		op = opmetalist_resolve(l, current_service->opfixups);
		i = oplisttab_add(op);
		$$ = opmeta_make(3, OP_WRAP, stringtab_add($2), i);
		free($2);
		oplist_free(op);
		free(op);
		opmetalist_free(l);
		free(l);
	}
	|	T_WRAP ';'
	{
		struct opmetalist *l;
		struct opmeta *o;
		int i;
		struct oplist *op;

		l = opmetalist_new();
		opmetalist_add(l, opmeta_make(1, OP_EXIT));
		op = opmetalist_resolve(l,current_service->opfixups);
		i = oplisttab_add(op);
		o = opmeta_make(3, OP_WRAP, 666, i);
		opmeta_fixup(o, 1, ofp_iname);
		$$ = o;
		oplist_free(op);
		free(op);
		opmetalist_free(l);
		free(l);
	}
	| T_CLOSE ';'
	{
		$$ = opmeta_make(1, OP_CLOSE);
	}
	| T_EXIT ';'
	{
		$$ = opmeta_make(1, OP_EXIT);
	}
	| T_ECHO T_QSTRING ';'
	{
		$$ = opmeta_make(2, OP_ECHO, argvtab_add($2, 0));
	}
	| T_LOOPBACK ';'
	{
		$$ = opmeta_make(29, OP_ZERO, OP_BUFINIT, 11, OP_RHOOK, 0, OP_BUFREAD,
										 OP_RUNHOOK, OP_DUP, OP_BZNEG, 17, OP_ADD, OP_WHOOK, 0,
										 OP_BUFWRITE, OP_WUNHOOK, OP_DUP, OP_BZ, 6, OP_DUP,
										 OP_BZNEG, 6, OP_SUB, OP_WHOOK, -11, OP_POP, OP_RHOOK, -22,
										 OP_JUMP, 666);
		opmeta_fixup($$, 28, ofp_onexit);
		current_service->internal = 1;
	}
	| T_DISCARD ';'
	{
		$$ = opmeta_make(13, OP_ZERO, OP_RHOOK, 0, OP_RUNHOOK, OP_BUFINIT, 1024,
										 OP_BUFREAD, OP_BZNEG, 2, OP_RHOOK, -8, OP_JUMP, 666);
		opmeta_fixup($$, 12, ofp_onexit);
		current_service->internal = 1;
	}
	;

optionalstring:
	{	$$ = NULL;	}
	|	T_QSTRING
	;

service_element:	';'
	|	T_PORT portlists ';'
	{
		if(current_service->port)
			stringlist_free(current_service->port);
		current_service->port = stringlist;
		stringlist = NULL;
	}
	|	T_PROTO protocol ';'
	{
		current_service->socktype = $2;
	}
	|	T_BACKLOG T_NUMERIC ';'
	{
		current_service->backlog = $2;
	}
	|	T_INSTANCES T_NUMERIC ';'
	{
		current_service->limit = $2;
	}
	| T_WAIT boolean ';'
	{
		current_service->wait  = $2;
	}
	| T_ENABLED boolean ';'
	{
		current_service->disabled  = ! $2;
	}
	|	T_INTERFACE ipaddrlists ';'
	{
		if(current_service->interface)
			stringlist_free(current_service->interface);
		current_service->interface = stringlist;
		stringlist = NULL;
	}
	|	T_INTERFACE T_ANY ';'
	{
		if(current_service->interface)
			stringlist_free(current_service->interface);
		current_service->interface = NULL;
	}
	|	T_SERVER T_QSTRING ';'
	{
		current_service->opfixups[ofp_exec] = stringtab_add($2);
		free($2);
	}
	|	T_RPC '{' rpcents '}'
	| T_FAMILY family ';'
	| T_FILTER T_QSTRING ';'
	{
#ifdef HAVE_NET_BPF_H
		if(current_service->filter)
			free(current_service->filter);
		if(rl_readfile($2, &current_service->filter, &current_service->filterlen))
			current_service->disabled++;
		free($2);
#else
		RL_PFATAL(EX_DATAERR, _("ABORT - support for socket filter not compiled in"));
#endif
	}

	;

rpcents:	rpcent
	|	rpcents rpcent
	;

rpcent:	T_NAME T_QSTRING ';'
	{
		current_service->rpcname = $2;
	}
	|	T_VERSION numrangelist ';'
	{
		current_service->rpcvers = numlist;
		numlist = NULL;
	}
	;

numrangelist:	numrange
	|	numrangelist ',' numrange
	;

numrange:	T_NUMERIC
	{
		numlist_add(&numlist, $1);
	}
	|	T_NUMERIC '-' T_NUMERIC
	{
		int i;

		if($3 >= $1)
			for(i = $1; i <= $3; i++)
				numlist_add(&numlist, i);
	}
	;

limitents:	limitent
	|	limitents limitent
	;

limitent:	T_SOFT limitvalue ';'
	{
		current_service->r.rlim_cur = $2;
	}
	|	T_HARD limitvalue ';'
	{
		current_service->r.rlim_max = $2;
	}
	;

ipaddrlists:	ipaddrlist
	|	ipaddrlists ',' ipaddrlist
	;

ipaddrlist:	T_IPADDR
	{
		stringlist_add(&stringlist, $1);
	}
	;

portlists:	portlist
	|	portlists ',' portlist
	;

portlist:	T_QSTRING
	{
		stringlist_add(&stringlist, $1);
	}
	| T_NUMERIC
	{
		char tmp[20];

		snprintf(tmp, 19, "%ld", $1);
		tmp[19] = '\0';
		stringlist_add(&stringlist, strdup(tmp));
	}
	;

limit:	limitvalue ';'
	{
		$$ = rlimittab_add($1, $1);
	}
	|	'{' limitents '}'
	{
		if((current_service->r.rlim_max != RLIM_INFINITY) &&
			 (current_service->r.rlim_cur > current_service->r.rlim_max)) {
			RL_PWARN(_("invalid resource limit"));
		}
		$$ = rlimittab_add(current_service->r.rlim_cur, current_service->r.rlim_max);
		current_service->r.rlim_cur = current_service->r.rlim_max = RLIM_INFINITY;
	}
	;

limitvalue:	T_NUMERIC
	{ $$ = (rlim_t)$1; }
	|	T_LIM_INFINITY
	{ $$ = RLIM_INFINITY; }
	;

limittype:	T_LIM_CPU
	{
#ifdef RLIMIT_CPU
		$$ = RLIMIT_CPU;
#else
		RL_PFATAL(EX_DATAERR, _("%s limit not available on this system"), "cpu");
#endif
	}

	|	T_LIM_FSIZE
	{
#ifdef RLIMIT_FSIZE
		$$ = RLIMIT_FSIZE;
#else
		RL_PFATAL(EX_DATAERR, _("%s limit not available on this system"), "fsize");
#endif
	}

	|	T_LIM_DATA
	{
#ifdef RLIMIT_DATA
		$$ = RLIMIT_DATA;
#else
		RL_PFATAL(EX_DATAERR, _("%s limit not available on this system"), "data");
#endif
	}

	|	T_LIM_STACK
	{
#ifdef RLIMIT_STACK
		$$ = RLIMIT_STACK;
#else
		RL_PFATAL(EX_DATAERR, _("%s limit not available on this system"), "stack");
#endif
	}

	|	T_LIM_CORE
	{
#ifdef RLIMIT_CORE
		$$ = RLIMIT_CORE;
#else
		RL_PFATAL(EX_DATAERR, _("%s limit not available on this system"), "core");
#endif
	}

	|	T_LIM_RSS
	{
#ifdef RLIMIT_RSS
		$$ = RLIMIT_RSS;
#else
		RL_PFATAL(EX_DATAERR, _("%s limit not available on this system"), "rss");
#endif
	}

	|	T_LIM_NPROC
	{
#ifdef RLIMIT_NPROC
		$$ = RLIMIT_NPROC;
#else
		RL_PFATAL(EX_DATAERR, _("%s limit not available on this system"), "nproc");
#endif
	}

	|	T_LIM_NOFILE
	{
#ifdef RLIMIT_NOFILE
		$$ = RLIMIT_NOFILE;
#else
		RL_PFATAL(EX_DATAERR, _("%s limit not available on this system"), "nofile");
#endif
	}

	|	T_LIM_MEMLOCK
	{
#ifdef RLIMIT_MEMLOCK
		$$ = RLIMIT_MEMLOCK;
#else
		RL_PFATAL(EX_DATAERR, _("%s limit not available on this system"), "memlock");
#endif
	}
	;

userid:		T_NUMERIC
	{
		$$ = getpwuid($1);
		endpwent();
	}
	|	T_QSTRING
	{
		$$ = getpwnam($1);
		free($1);
		endpwent();
	}
	;

groupid:	T_NUMERIC
	{
		$$ = getgrgid($1);
		endgrent();
	}
	|	T_QSTRING
	{
		$$ = getgrnam($1);
		free($1);
		endgrent();
	}
	;

protocol:	T_TCP
	{
		$$ = SOCK_STREAM;
		current_service->protoname = "tcp";
		current_service->proto = IPPROTO_TCP;
	}
	|	T_UDP
	{
		$$ = SOCK_DGRAM;
		current_service->protoname = "udp";
		current_service->proto = IPPROTO_UDP;
	}
	;

family:	T_IPV4
	{
		current_service->family = PF_INET;
	}
	| T_IPV6
	{
#ifdef HAVE_SOCKADDR_IN6
		current_service->family = PF_INET6;
#else
		RL_PWARN(_("ipv6 support is not compiled in"));
		current_service->family = PF_INET;
#endif
	}
	;

signed_numeric: T_NUMERIC
{
		$$ = $1;
	}
	| '-' T_NUMERIC
	{
		$$ = (-$2);
	}
	;

boolean: T_YES
	{
		$$ = 1;
	}
	| T_NO
	{
		$$ = 0;
	}
	;
%%

void yyerror(const char *str) {
	RL_PFATAL(EX_DATAERR, _("ABORT - %s"), str);
}

int yywrap(void) {
	fclose(yyin);
	if(numfiles) {
		while(++curfile < numfiles) {
			if((yyin = fopen(files[curfile], "r"))) {
				curfile_name = files[curfile];
				curfile_line = 1;
				return 0;
			} else {
				curfile_name = NULL;
				RL_PWARN(_("cannot open file %s (%s)"), files[curfile],
						strerror(errno));
			}
		}
	}
	return 1;
}

static struct service *service_new() {
	struct service *p = (struct service *)malloc(sizeof(*p));

	if (!p)
		rl_fatal(EX_SOFTWARE, _("ABORT - Can't allocate memory"));
	memset(p, 0, sizeof(*p));
	return p;
}

void services_free() {
	if(rl_cleanups) {
		rlp_cleanup(rl_cleanups);
		rl_cleanups = NULL;
	}
}

static void validate_service(struct service *s) {
	if(!s->name) {
		RL_PWARN(_("service without name"));
		s->disabled++;
		return;
	}

	if (!has_flag(s, FLAG_USER)) {
		RL_PWARN(_("user is not set for service %s"), s->name);
		s->disabled++;
		return;
	}

	if (s->wait && s->limit != 1) {
		if (s->limit != defaults->limit)
			RL_PWARN(_("ignoring `instances %d' directive for service %s, because `wait' was also given"),
										s->limit, s->name);
		s->limit = 1;
	}

	if(s->rpcvers) {
		const char *rpcname = s->name;
		struct rpcent *rep;

		if(s->rpcname)
			rpcname = s->rpcname;
		rep = getrpcbyname(rpcname);
		if(!rep) {
			RL_PWARN(_("can't find rpc service %s for service %s"),
							rpcname, s->name);
			s->disabled++;
		} else {
			s->rpcnum = rep->r_number;
		}
		endrpcent();
	}
}

static in_port_t get_rpc_port(struct service * cur_service)
{
	if (!cur_service->rpcnum)
		return 0;
	const struct numlist *vers = cur_service->rpcvers;
	struct sockaddr_in myaddress;
	get_myaddress(&myaddress);
	in_port_t result = 0;
	do {
		const in_port_t port = pmap_getport(&myaddress, cur_service->rpcnum, vers->num,
																				cur_service->proto);
		if (rl_debug > 1)
			rl_note("++ pmap_getport(?, %d, %d, %d) for service %s returned %d",
												cur_service->rpcnum, vers->num, cur_service->proto,
												cur_service->name, port);
		if (result && port && result != port) {
			RL_PWARN("rpc service %s already registered on different ports: %d and %d",
								cur_service->name, port, result);
			++cur_service->disabled;
		} else if (port) {
			result = port;
		}
	} while((vers = vers->next));

	return result;
}

static int register_rpc_service(int * rpc_registered,
																const struct service * const cur_service,
																const in_port_t rpc_port)
{
	if (*rpc_registered)
		return 1;

	const struct numlist *vers = cur_service->rpcvers;

	do {
		if (rl_debug > 1)
			rl_note("++ calling pmap_set(%d, %d, %d, %d) for service %s",
							cur_service->rpcnum, vers->num, cur_service->proto, rpc_port,
							cur_service->name);

		if(pmap_set(cur_service->rpcnum, vers->num, cur_service->proto, rpc_port))
			++*rpc_registered;
		else
			RL_PWARN(_("pmap_set(%d, %d, %d, %d) failed for service %s"),
				cur_service->rpcnum, vers->num, cur_service->proto, rpc_port,
				cur_service->name);
		} while((vers = vers->next));

		if (*rpc_registered) {
			struct rl_cleanup *p = (struct rl_cleanup *)malloc(sizeof(struct rl_cleanup));
			if (!p)
				rl_fatal(EX_SOFTWARE, _("ABORT - Can't allocate memory"));
			p->next = rl_cleanups;
			p->data =	(struct rlc_unrpc *)malloc(sizeof(struct rlc_unrpc));
			if (!p->data)
				rl_fatal(EX_SOFTWARE, _("ABORT - Can't allocate memory"));
			p->type = RLC_UNRPC;
			((struct rlc_unrpc *)p->data)->vers = NULL;
			numlist_copy(&(((struct rlc_unrpc *)p->data)->vers), cur_service->rpcvers);
			((struct rlc_unrpc *)p->data)->prog =	cur_service->rpcnum;
			rl_cleanups = p;
		}
		else if (rl_debug > 1)
			rl_note("++pmap_set() failed for all versions of service %s", cur_service->name);
		return *rpc_registered;
}

static int bind_ports() {
	int fd;
	long opt = 1;
	struct stringlist *portp;
	struct stringlist *ifp;
#ifdef HAVE_GETADDRINFO
	struct addrinfo hints;
	struct addrinfo *results = NULL, *ai;
	int ret;
#else
	void *addrbuf;
	int portnum;
	char *ctmp;
#endif
	const char *port, *addr;
	const char *lasterror;
	const char *errfunc = NULL;
	int succeeded;
	fd_set fds;
	int n_fds;

	int family, socktype, protocol;
	socklen_t saddrlen;
	struct sockaddr *saddr = NULL;
	int rpc_registered = 0;
	in_port_t rpc_port = get_rpc_port(current_service);

	if (current_service->disabled)
		return 0;

	FD_ZERO(&fds);
	n_fds = 0;
	ifp = current_service->interface;
	do {
		addr = ifp ? ifp->str : NULL;
		portp = current_service->port;
		do {
			lasterror = NULL;
			succeeded = 0;

			port = portp ? portp->str : !current_service->rpcnum ? current_service->name : "0";
#ifdef HAVE_GETADDRINFO
			memset(&hints, 0, sizeof(hints));
			hints.ai_flags = AI_PASSIVE;
			hints.ai_family = current_service->family;
			hints.ai_socktype = current_service->socktype;
			hints.ai_protocol = current_service->proto;
			if(results)
				freeaddrinfo(results);
			results = NULL;
			if((ret = getaddrinfo(addr, port, &hints, &results))) {
#ifdef HAVE_GAI_STRERROR
				RL_PWARN(_("getaddrinfo(%s, %s) failed: %s"), addr, port,
								gai_strerror(ret));
#else
				RL_PWARN(_("getaddrinfo(%s, %s) failed: %s"), addr, port,
								_("for reasons unknown") );
#endif
				continue;
			}
			ai = results;
			do {
				family = ai->ai_family;
				socktype = ai->ai_socktype;
				protocol = ai->ai_protocol;
				saddr = ai->ai_addr;
				saddrlen = ai->ai_addrlen;
#else
				portnum = strtoul(port, &ctmp, 10);
				if(*ctmp) {
					portnum = getservport(port, current_service->protoname);
					if(portnum == -1) {
						RL_PWARN(_("Failed to resolve %s, protocol %s"),
										current_service->proto, port);
						continue;
					}
				}
				family = current_service->family;
				socktype = current_service->socktype;
				protocol = current_service->proto;
				if(saddr) {
					free(saddr);
					saddr = NULL;
				}
				switch(family) {
					struct sockaddr_in *sin;
#ifdef HAVE_SOCKADDR_IN6
					struct sockaddr_in6 *sin6;
#endif

					case PF_INET:
						saddrlen = sizeof(struct sockaddr_in);
						sin = (struct sockaddr_in *)malloc(saddrlen);
						if (!sin)
							rl_fatal(EX_SOFTWARE, _("ABORT - Can't allocate memory"));
						addrbuf = &sin->sin_addr.s_addr;
						sin->sin_family = current_service->family;
						sin->sin_port = htons(portnum);
						sin->sin_addr.s_addr = INADDR_ANY;
						saddr = (struct sockaddr *)sin;
						break;
#ifdef HAVE_SOCKADDR_IN6
					case PF_INET6:
						saddrlen = sizeof(struct sockaddr_in6);
						sin6 = (struct sockaddr_in6 *)malloc(saddrlen);
						if (!sin6)
							rl_fatal(EX_SOFTWARE, _("ABORT - Can't allocate memory"));
						addrbuf = &sin6->sin_addr.s_addr;
						sin6->sin6_family = current_service->family;
						sin6->sin6_port = htons(portnum);
						sin6->sin6_addr = IN6ADDR_ANY_INIT;
						saddr = (struct sockaddr *)sin6;
						break;
#endif
					default:
						RL_PFATAL(EX_SOFTWARE, _("funky family in service"));
				}
				if(addr) {
					if(
#ifdef HAVE_INET_PTON
						 inet_pton(family, addr, addrbuf)
#else
																/* this cast is a pain */
						 (*(unsigned int *)addrbuf = inet_addr(addr)) == -1
#endif
						 ) {
						RL_PWARN(_("bad address %s in service %s"), addr,
										current_service->name);
						free(saddr);
						saddr = NULL;
						continue;
					}
				}
#endif
				if((fd = socket(family, socktype, protocol)) < 0) {
					lasterror = strerror(errno);
					errfunc = "socket()";
					goto out;
				}
				if(setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt))) {
					RL_PWARN(_("setsockopt(%d, SOL_SOCKET, SO_REUSEADDR, 1) failed for service %s (%s)"),
									fd, current_service->name, strerror(errno));
				}
#ifdef HAVE_NET_BPF_H
				if(current_service->filter) {
					struct bpf_program bp;

					bp.bf_len = current_service->filterlen / sizeof(struct bpf_insn);
					bp.bf_insns = current_service->filter;
					if(setsockopt(fd, SOL_SOCKET, SO_ATTACH_FILTER, &bp, sizeof(bp))) {
						lasterror = strerror(errno);
						close(fd);
						errfunc = "setsockopt(..., SO_ATTACH_FILTER)";
						goto out;
					}
				}
#endif
				/* For rpc service bind to the same port as it was already registered */
				if(current_service->rpcnum && rpc_port) {
					/* Note: this should be either sockaddr_in or sockaddr_in6, but fortunatelly
							the port is at the same place in both structures. */
					((struct sockaddr_in *)saddr)->sin_port = htons(rpc_port);
				}

				if(bind(fd, saddr, saddrlen) < 0) {
					lasterror = strerror(errno);
					close(fd);
					errfunc = "bind()";
					goto out;
				}

				if(current_service->rpcnum) {

					if (!rpc_port) {
						if (getsockname(fd, saddr, &saddrlen) == -1) {
							lasterror = strerror(errno);
							close(fd);
							errfunc = "getsockname()";
							goto out;
						}
						/* Note: this should be either sockaddr_in or sockaddr_in6, but fortunatelly
								the port is at the same place in both structures. */
						rpc_port = ntohs(((struct sockaddr_in *)saddr)->sin_port);
						if (!rpc_port) { /* shouldn't happen */
								lasterror = "returned port is 0";
								close(fd);
								errfunc = "getsockname()";
								goto out;
						}
					}

					if(!register_rpc_service(&rpc_registered, current_service, rpc_port)) {
						close(fd);
						goto out_all_loops;
					}
				}
				FD_SET(fd, &fds);
				if(socktype == SOCK_STREAM) {
					if(listen(fd, current_service->backlog)) {
						lasterror = strerror(errno);
						close(fd);
						errfunc = "listen(fd)";
						goto out;
					}
				}
				if(fcntl(fd, F_SETFD, 1)) {
					lasterror = strerror(errno);
					errfunc = "fcntl(fd, F_SETFD, 1)";
					close(fd);
					goto out;
				}
				if(fcntl(fd, F_SETFL, O_NDELAY)) {
					lasterror = strerror(errno);
					close(fd);
					errfunc = "fcntl(fd, F_SETFL, O_NDELAY)";
					goto out;
				}
				n_fds++;
				succeeded++;
			out:
#ifdef HAVE_GETADDRINFO
				continue;
			} while((ai = ai->ai_next));
#endif
			if(!succeeded && lasterror)
				RL_PWARN(_("%s failed for service %s: %s"),
								errfunc, current_service->name, lasterror);
		} while(portp && (portp = portp->next));
	} while(ifp && (ifp = ifp->next));
out_all_loops:
	stringlist_free(current_service->port);
	current_service->port = NULL;
	stringlist_free(current_service->interface);
	current_service->interface = NULL;
	numlist_free(current_service->rpcvers);
	current_service->rpcvers = NULL;
#ifdef HAVE_GETADDRINFO
	if(results) {
		freeaddrinfo(results);
		results = NULL;
	}
#else
	if(saddr) {
		free(saddr);
		saddr = NULL;
	}
#endif
	if (!n_fds)
		current_service->disabled++;
	return fdsettab_add(&fds);
}

static int add_user_group(const struct service* s, struct opmetalist * l)
{
	struct opmeta *o = NULL;
	if (has_flag(s, FLAG_USER))
	{
		o = opmeta_make(2, OP_SUID, 666);
		opmeta_fixup(o, 1, ofp_setuid);
		if(opmetalist_add(l, o)) {
				RL_PWARN(_("opcode resolving problem"));
				return 0;
		}
	}

	if (has_flag(s, FLAG_USER) || (has_flag(s, FLAG_GROUP)))
	{
				o = opmeta_make(2, OP_SGID, 666);
				opmeta_fixup(o, 1, ofp_setgid);
				if(opmetalist_add(l, o)) {
					RL_PWARN(_("opcode resolving problem"));
				  return 0;
				}
	}

	if (has_flag(s, FLAG_USER) && has_flag(s, FLAG_INITGROUPS))
	{
			o = opmeta_make(3, OP_INITGR, 666, 666);
			opmeta_fixup(o, 1, ofp_iuser);
			opmeta_fixup(o, 2, ofp_supgid);
			if(opmetalist_add(l, o)) {
				RL_PWARN(_("opcode resolving problem"));
			 return 0;
		}
	}
	return 1;
}

static void service_free(struct service *s) {
	if(s->name) {
		free(s->name);
		s->name = NULL;
	}
	stringlist_free(s->port);
	s->port = NULL;
	stringlist_free(s->interface);
	s->interface = NULL;
	if(s->rpcname) {
		free(s->rpcname);
		s->rpcname = NULL;
	}
	if(s->rpcvers) {
		numlist_free(s->rpcvers);
		s->rpcvers = NULL;
	}
#ifdef HAVE_CAPABILITIES
	if(s->caps) {
		cap_free(&s->caps);
		s->caps = NULL;
	}
#endif
#ifdef HAVE_NET_BPF_H
	if(s->filter) {
		free(s->filter);
		s->filter = NULL;
		s->filterlen = 0;
	}
#endif
}

static void service_copy(struct service *to, struct service *from) {
	memcpy(to, from, sizeof(*to));
	to->name = NULL;
	to->port = NULL;
	to->interface = NULL;
	stringlist_copy(&to->port, from->port);
	stringlist_copy(&to->interface, from->interface);
	to->rpcname = from->rpcname ? strdup(from->rpcname) : NULL;
	to->rpcvers = NULL;
	numlist_copy(&to->rpcvers, from->rpcvers);
#ifdef HAVE_LIBCAP
	to->caps = from->caps ? cap_dup(from->caps) : NULL;
#endif
#ifdef HAVE_NET_BPF_H
	if(from->filter) {
		to->filter = malloc(from->filterlen);
		if (!to->filter)
			rl_fatal(EX_SOFTWARE, _("ABORT - Can't allocate memory"));
		memcpy(to->filter, from->filter, from->filterlen);
	}
#endif
}

static struct logdata *logdata_get(char *name) {
	struct logdata *ptr = logdatas;

	if(!ptr)
		return NULL;
	do {
		if(!strcmp(name, ptr->name))
			return ptr;
	} while((ptr = ptr->next));
	return NULL;
}

static void logdatas_free(void) {
	struct logdata *p, *q;

	p = logdatas;
	while(p) {
		q = p->next;
		if(p->name)
			free(p->name);
		if(p->path)
			free(p->path);
		free(p);
		p = q;
	}
	logdatas = NULL;
}

static struct logdata *logdata_new() {
	struct logdata *ptr = (struct logdata *)malloc(sizeof(*ptr));

	if (!ptr)
		rl_fatal(EX_SOFTWARE, _("ABORT - Can't allocate memory"));

	memset(ptr, 0, sizeof(*ptr));
	ptr->index = -1;
	ptr->mode =
#ifdef S_IRUSR
		S_IRUSR|S_IWUSR|S_IRGRP
#else
		0640
#endif
		;
	ptr->uid = -1;
	ptr->gid = -1;
	return ptr;
}

extern void parse(void);
void parse(void) {

	numlist = NULL;
	userdata = NULL;
	newuserdata(&userdata);
	pidtab_fixup();
	all_unhook();
	logtabs_free();
	argvtabs_free();
	rlimittabs_free();
	services_free();
	stringtabs_free();
	buftabs_free();
	oplisttabs_free();
#ifdef HAVE_CAPABILITIES
	captabs_free();
#endif
	semaphores_free();
	fdsettabs_free();
	if(strcmp(rl_config, "-") || !rl_debug) {
		if(!(yyin = fopen(rl_config, "r"))) {
			RL_PFATAL(EX_NOINPUT, _("fopen(%s) failed, aborting"), rl_config);
		} else {
			curfile_name = rl_config;
			curfile_line = 1;
		}
	}

	logcur = logdata_new();
	current_service = service_new();
	defaults = service_new();
#ifdef HAVE_GETADDRINFO
	defaults->family = PF_UNSPEC;
#else
	defaults->family = PF_INET;
#endif
	defaults->socktype = SOCK_STREAM;
	defaults->protoname = "tcp";
	defaults->proto = IPPROTO_TCP;
	defaults->backlog = 5;
	defaults->limit = 40;
	defaults->r.rlim_cur = RLIM_INFINITY;
	defaults->r.rlim_max = RLIM_INFINITY;
	defaults->wait = 0;


	defaults->opfixups[ofp_exec] = -1;

	defaults->opfixups[ofp_iname] = -1;
	defaults->opfixups[ofp_parent] = -1;
	defaults->opfixups[ofp_iuser]  = -1;
	defaults->sflags =  0;
	defaults->opfixups[ofp_supgid] = -1;
	defaults->opfixups[ofp_setgid] = -1;
	defaults->opfixups[ofp_setuid] = -1;
	service_copy(current_service, defaults);
	 opml_defaults = opmetalist_new();
	yyparse();
	freebufs();
	service_free(defaults);
	free(defaults);
	defaults = NULL;
	logdatas_free();
	free(logcur);
	logcur = NULL;
	service_free(current_service);
	free(current_service);
	current_service = NULL;
	for(curfile = 0; curfile < numfiles; curfile++) {
		free(files[curfile]);
		files[curfile] = NULL;
	}
	free(files);
	files = NULL;
	clearuserdata(&userdata);
	free(userdata);
}

static void pidtab_fixup() {
	struct pidtab *p;
	int i;

	for(i = 0; i < 8; i++) {
		p = pidtabs[i].next;
		while(p) {
			if(p->inst)
				inst_free(p->inst);
			p->inst = NULL;
			p = p->next;
		}
	}
}

#ifndef HAVE_GETADDRINFO
static int getservport(char *str, char *proto) {
	struct servent *foo;
	int ret = -1;

	foo = getservbyname(str, proto);
	if(foo)
		ret = ntohs(foo->s_port);
	endservent();
	return ret;
}
#endif

static void add_directory(char *dir, char *match, char *ignore) {
	DIR *d;
	struct dirent *de;
	regex_t rmatch, rignore;
	char err[128];
	int e;
	struct stat st;
	char *file;

	if(match)
		if((e = regcomp(&rmatch, match, REG_EXTENDED|REG_NOSUB))) {
			regerror(e, &rmatch, err, 127);
			RL_PWARN(_("regexp compile failed for directory %s: %s"),
							dir, err);
		}
	if(ignore)
		if((e = regcomp(&rignore, ignore, REG_EXTENDED|REG_NOSUB))) {
			regerror(e, &rignore, err, 127);
			RL_PWARN(_("regexp compile failed for directory %s: %s"),
							dir, err);
		}

	if((d = opendir(dir))) {
		while((de = readdir(d))) {
			if(match)
				if(regexec(&rmatch, de->d_name, 0, NULL, 0))
					continue;
			if(ignore)
				if(!regexec(&rignore, de->d_name, 0, NULL, 0))
					continue;
			if(de->d_name[0] != '.') {
				file = malloc(strlen(dir) + NAMLEN(de) + 2);
				if (!file)
					rl_fatal(EX_SOFTWARE, _("ABORT - Can't allocate memory"));
				file[0] = '\0';
				strcat(file, dir);
				strcat(file, "/");
				strcat(file, de->d_name);
				if(stat(file, &st) || S_ISDIR(st.st_mode)) {
					RL_PWARN(_("file %s does not exists or"
						 " is a directory"), file);
					free(file);
					continue;
				}
				files = (char **)realloc(files, ++numfiles * sizeof(char *));
				if (!files)
					rl_fatal(EX_SOFTWARE, _("ABORT - Can't allocate memory"));
				files[numfiles - 1] = file;
			}
		}
	} else {
		RL_PFATAL(EX_NOINPUT, _("Directory %s open failed (%s)"), dir,
			strerror(errno));
	}
	closedir(d);
	free(dir);
	if(match) {
		free(match);
		regfree(&rmatch);
	}
	if(ignore) {
		free(ignore);
		regfree(&rignore);
	}
}

int chargen_buffer() {
	static int cb = -1;
	const char * const b = "01234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ\r\n";

	if(cb == -1)
		cb = buftab_addbuf(b, strlen(b));
	return cb;
}


/* vim: set ts=2 sts=2 noet: */
