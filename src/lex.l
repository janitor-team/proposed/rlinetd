/* Prevent compilation of static input() and unput() functions in generated scanner
   code to avoid gcc 4.x `defined but not used' warnings
*/
%option noinput
%option nounput
%option never-interactive
%{
#include "config.h"

#include <stdlib.h>
#include <string.h>
#include <sysexits.h>
#include <syslog.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/time.h>
#include <sys/resource.h>

#include "port/port.h"
#include "grammar.h"
#include "error.h"
#include "parse.h"
#include "lex.h"

void yyerror(char *c);
%}

%%


service	return(T_SERVICE);
exec	return(T_EXEC);
port	return(T_PORT);
protocol	return(T_PROTO);
tcp	return(T_TCP);
udp	return(T_UDP);
user	return(T_UID);
group	return(T_GID);
backlog	return(T_BACKLOG);
instances	return(T_INSTANCES);
defaults	return(T_DEFAULT);
nice	return(T_NICE);
rlimit	return(T_RLIMIT);
cpu	return(T_LIM_CPU);
fsize	return(T_LIM_FSIZE);
data	return(T_LIM_DATA);
stack	return(T_LIM_STACK);
core	return(T_LIM_CORE);
rss	return(T_LIM_RSS);
nproc	return(T_LIM_NPROC);
nofile	return(T_LIM_NOFILE);
memlock	return(T_LIM_MEMLOCK);
unlimited	return(T_LIM_INFINITY);
interface	return(T_INTERFACE);
log	return(T_LOG);
fish	return(T_FISH);
soft	return(T_SOFT);
hard	return(T_HARD);
chroot	return(T_CHROOT);
pipe	return(T_PIPE);
syslog	return(T_SYSLOG);
path	return(T_PATH);
file	return(T_PATH);
mode	return(T_MODE);
server	return(T_SERVER);
directory	return(T_DIR);
rpc	return(T_RPC);
version	return(T_VERSION);
name	return(T_NAME);
tcpd	return(T_WRAP);
capability	return(T_CAPS);
family	return(T_FAMILY);
ipv4	return(T_IPV4);
ipv6	return(T_IPV6);
initgroups	return(T_INITGROUPS);
accept	return(T_ACCEPT);
deny	return(T_DENY);
close	return(T_CLOSE);
banner	return(T_BANNER);
exit	return(T_EXIT);
echo	return(T_ECHO);
discard	return(T_DISCARD);
filter	return(T_FILTER);
chargen	return(T_CHARGEN);
loopback	return(T_LOOPBACK);
wait	return(T_WAIT);
enabled return(T_ENABLED);
yes	return(T_YES);
no	return(T_NO);
any return(T_ANY);

\{	return '{';
\}	return '}';
;	return ';';
,	return ',';
-	return '-';

\n		{
			curfile_line++;
		}
#[^\n]*
\"[^\"]*\"	{
			yylval.cp = malloc(yyleng - 1);
			memcpy(yylval.cp, yytext + 1, yyleng - 2);
			yylval.cp[yyleng - 2] = '\0';
			return(T_QSTRING);
		}
([[:digit:]]{1,3}"."){1,3}([[:digit:]]{1,3})? {
			yylval.cp = strdup(yytext);
			return(T_IPADDR);
		}
[[:digit:]]+	{
			yylval.num = strtol(yytext, NULL, 0);
			return(T_NUMERIC);
		}
[[:xdigit:]:]+	{
			yylval.cp = strdup(yytext);
			return(T_IPADDR);
		}
[:blank:]+
[[:alnum:]]+	{
			rl_pwarn(curfile_name, curfile_line,
				"unknown directive: %s", strdup(yytext));
		}

%%

void freebufs(void) {
	yy_delete_buffer(YY_CURRENT_BUFFER);
}

/* vim: set ts=2: */
