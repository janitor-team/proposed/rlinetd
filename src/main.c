#include "config.h"
#include "installpaths.h"

#include <errno.h>
#include <fcntl.h>
#include <grp.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdarg.h>
#include <sysexits.h>
#include <syslog.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>

#if TIME_WITH_SYS_TIME
# include <sys/time.h>
# include <time.h>
#else
# if HAVE_SYS_TIME_H
#  include <sys/time.h>
# else
#  include <time.h>
# endif
#endif

#ifdef HAVE_GETOPT_H
#include <getopt.h>
#endif

#include <sys/resource.h>

#include "bytecode.h"
#include "db.h"
#include "engine.h"
#include "rlinetd.h"
#include "signals.h"

int rl_debug = 0;
const char * rl_parser = MODULESDIR "/libparse.so";
const char * rl_config = SYSCONFDIR "/rlinetd.conf";
char rl_lf[] = "\n";
#ifdef HAVE_GETOPT_LONG
struct option options[] = {
		{ "help", 0, NULL, 'h' },
		{ "debug", 0, NULL, 'd' },
		{ "parser", 1, NULL, 'p' },
		{ "config", 1, NULL, 'f' },
		{ NULL, 0, NULL, 0 }
};
#endif

int main(int argc, char **argv) {
	int i;

#if ENABLE_NLS
  setlocale(LC_ALL, "");
  bindtextdomain(PACKAGE, LOCALEDIR);
  textdomain(PACKAGE);
#endif

#ifdef HAVE_GETOPT
	while((i =
#ifdef HAVE_GETOPT_LONG
				 getopt_long
#else
				 getopt
#endif
				 (argc, argv, "p:f:dh"
#ifdef HAVE_GETOPT_LONG
												 , options, NULL
#endif
												 )) != -1) {
		switch(i) {
			case 'p':
				rl_parser = optarg;
				break;
			case 'f':
				rl_config = optarg;
				break;
			case 'd':
				rl_debug++;
				break;
			case 'h':
				fprintf(stderr, _("%s Version %s\n"), PACKAGE, VERSION);
				fprintf(stderr, "\n");
				fprintf(stderr, _("Usage:\n"));
				fprintf(stderr, _("%s [options]\n"), argv[0]);
				fprintf(stderr, "\n");
				fprintf(stderr, _("Options:\n"));
				fprintf(stderr, _("  -p|--parser file    alternative parser module (default %s)\n"), rl_parser);
				fprintf(stderr,	_("  -f|--config file    alternative config file (default %s)\n"), rl_config);
				fprintf(stderr,	_("  -d|--debug          enable debugging\n"));
				fprintf(stderr,	_("  -h|--help           this message\n"));
				exit(0);
			default:
				/* getopt should print error message */
				exit(EX_USAGE);
		}
	}
#endif
	if(!rl_debug) {
		if (daemon(0, 0) == -1)
		{
			perror("daemon");
		}
	}
	setgroups(0, NULL);
	openlog("rlinetd", LOG_NDELAY|LOG_PID, LOG_DAEMON);
	rl_siginit();
	main_loop();
	return 0;
}

/* vim: set ts=2: */
