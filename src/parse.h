#ifndef H_PARSE
#define H_PARSE

#include <sys/types.h>

#include "rlinetd.h"

struct logdata {
	int index;
	struct logdata *next;
	char *name;
	char *path;
	mode_t mode;
	uid_t uid;
	gid_t gid;
};

void sockettabs_free(void);
void services_free(void);

void rlp_cleanup(struct rl_cleanup *rlc);

enum { RLC_UNRPC };

struct rlc_unrpc {
	struct numlist *vers;
	long prog;
};

extern char *curfile_name;
extern int  curfile_line;

			
#endif /* !H_PARSE */

/* vim: set ts=2: */
