#ifndef H_RLINETD
#define H_RLINETD

#include <signal.h>
#include <syslog.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/resource.h>
#include <unistd.h>
#include <netinet/in.h>

#include "buffer.h"
#include "bytecode.h"
#include "stack.h"
#include "config.h"
#include "gettext.h"

#define _(String) gettext (String)
#define N_(String) gettext_noop (String)

#ifdef __GNUC__
#define UNUSED __attribute__((unused))
#else
#define UNUSED
#endif


struct rl_cleanup {
	int type;
	void *data;
	struct rl_cleanup *next;
};

struct rl_instance {
	struct sockaddr *sin;
	socklen_t sinlen;
	struct rusage rusage;
	int status;
	time_t start;
	volatile time_t stop;
	int sock;
	struct rl_buffer *buf;
	struct rl_stack stk;
};

extern int rl_debug;

void run_bytecode(rl_opcode_t *, struct rl_instance *);

extern struct rl_cleanup *rl_cleanups;

void read_hook(int, rl_opcode_t *, struct rl_instance *);
void read_unhook(int);
void write_hook(int, rl_opcode_t *, struct rl_instance *);
void write_unhook(int);
void all_unhook(void);

void listeners_clear(int);
void listeners_set(int);

void inst_free(struct rl_instance *);

#endif /* !H_RLINETD */

/* vim: set ts=2: */
