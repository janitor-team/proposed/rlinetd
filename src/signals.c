#include "config.h"

#include <errno.h>
#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <sysexits.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <time.h>

#include "db.h"
#include "error.h"
#include "rlinetd.h"
#include "signals.h"

volatile int rls_need_parse = 1;
volatile int rls_term_recv  = 0;
struct pidtab * volatile rls_reaped = NULL;
static sigset_t rls_atomic;

void rl_reap(int);
void rl_hup(int);
void rl_term(int);

void rl_siginit() {
	struct sigaction sa;

	sigemptyset(&rls_atomic);
	sigaddset(&rls_atomic, SIGCHLD);
	sigaddset(&rls_atomic, SIGHUP);

	sigemptyset(&sa.sa_mask);
	sa.sa_handler = SIG_IGN;
	if(sigaction(SIGPIPE, &sa, NULL)) {
		rl_fatal(EX_OSERR, _("ABORT - sigaction(%s) failed"), "SIGPIPE");
	}
	sa.sa_handler = rl_reap;
	sa.sa_flags = SA_NOCLDSTOP;
	if(sigaction(SIGCHLD, &sa, NULL)) {
		rl_fatal(EX_OSERR, _("ABORT - sigaction(%s) failed"), "SIGCHLD");
	}
	sa.sa_handler = rl_hup;
	if(sigaction(SIGHUP, &sa, NULL)) {
		rl_fatal(EX_OSERR, _("ABORT - sigaction(%s) failed"), "SIGHUP");
	}
	sa.sa_handler = rl_term;
	if(sigaction(SIGTERM, &sa, NULL)) {
		rl_fatal(EX_OSERR, _("ABORT - sigaction(%s) failed"), "SIGTERM");
	}
	sa.sa_handler = rl_term;
	if(sigaction(SIGINT, &sa, NULL)) {
		rl_fatal(EX_OSERR, _("ABORT - sigaction(%s) failed"), "SIGINT");
	}
}

void rl_reap(int i UNUSED) {
	int status;
	pid_t pid;
	struct rusage rusage;
	struct pidtab *p;
	
	for(;;) {
		switch(pid = wait3(&status, WNOHANG, &rusage)) {
			case -1:
				switch(errno) {
					case ECHILD:
						return;
					case EINTR:
						continue;
					default:
						rl_warn(_("wait3 failed"));
						return;
				}
			case 0:
				return;
			default:
				p = pidtab_get(pid);
				if(!p)
					continue;
				p->next_cleanup = rls_reaped;
				rls_reaped = p;
				if(!p->inst)
					continue;
				p->inst->status = status;
				p->inst->stop = time(NULL);
				memcpy(&p->inst->rusage, &rusage, sizeof(rusage));
				continue;
		}
	}
}

void rl_hup(int i UNUSED) {
	rls_need_parse = 1;
}

void rl_term(int i UNUSED) {
	rls_term_recv = 1;
}


void rls_block() {
	sigprocmask(SIG_BLOCK, &rls_atomic, NULL);
}

void rls_unblock() {
	sigprocmask(SIG_UNBLOCK, &rls_atomic, NULL);
}

/* vim: set ts=2: */
