#include "config.h"

#include <stdlib.h>

#include "bytecode.h"
#include "error.h"
#include "stack.h"
#include "rlinetd.h"

void rlstk_push(struct rl_stack *s, rl_opcode_t val) {
	if(s->top == STACKSIZE)
		rl_fatal(EX_SOFTWARE, _("Stack overflow"));
	s->data[s->top++] = val;
}

rl_opcode_t rlstk_pop(struct rl_stack *s) {
	if(!s->top)
		rl_fatal(EX_SOFTWARE, _("Stack underflow"));
	return s->data[--s->top];
}

rl_opcode_t rlstk_peek(struct rl_stack *s, int offset) {
	if(offset >= s->top)
		rl_fatal(EX_SOFTWARE, _("Stack peek undefined"));
	return s->data[s->top - offset - 1];
}

void rlstk_poke(struct rl_stack *s, int offset, rl_opcode_t val) {
	if(offset >= s->top)
		rl_fatal(EX_SOFTWARE, _("Stack peek undefined"));
	s->data[s->top - offset - 1] = val;
}

struct rl_stack *rlstk_new() {
	struct rl_stack *s;

	s = malloc(sizeof(*s));
	if (!s)
		rl_fatal(EX_SOFTWARE, _("ABORT - Can't allocate memory"));
	memset(s, 0, sizeof(*s));
	return s;
}

void rlstk_free(struct rl_stack *s) {
	free(s);
}

/* vim: set ts=2: */
