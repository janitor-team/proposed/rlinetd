#ifndef H_STRINGFISH
#define H_STRINGFISH

#include "bytecode.h"
#include "db.h"
#include "rlinetd.h"

struct argvtab {
	int argc;
	struct loglist *ents;
	char **argv;
	struct iovec *iov;
	char *str;
};

void loglist_build(struct rl_instance *i, struct loglist *l);
void string_build(struct rl_instance *inst, struct argvtab *a);

#endif /* H_STRINGFISH */

/* vim: set ts=2: */
