#include "config.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>

#include "error.h"
#include "util.h"
#include "rlinetd.h"

int rl_readfile(char *path, void **addr, int *len) {
	int fd;
	struct stat st;
	void *tmp;
	
	if((fd = open(path, O_RDONLY)) < 0) {
		rl_warn(_("Failed to open %s (%s)"), path, strerror(errno));
		return -1;
	}
	if(fstat(fd, &st)) {
		rl_warn(_("Failed to fstat %d (%s)"), fd, strerror(errno));
		close(fd);
		return -1;
	}
	tmp = malloc(st.st_size);
	if (!tmp)
		rl_fatal(EX_SOFTWARE, _("ABORT - Can't allocate memory"));
	if(read(fd, tmp, st.st_size) < 0) {
		rl_warn(_("Failed to inhale file %s"), path);
		free(tmp);
		close(fd);
		return -1;
	}
	close(fd);
	*addr = tmp;
	*len = st.st_size;
	return 0;
}

/* vim: set ts=2: */
